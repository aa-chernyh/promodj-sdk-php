<?php

namespace PromodjSDKTests;

use ClientInterface\Exception\ValidationException;
use DiDom\Exceptions\InvalidSelectorException;
use PHPUnit\Framework\TestCase;
use PromodjSDK\Client;
use PromodjSDK\enums\Kinds;
use PromodjSDK\enums\Styles;
use PromodjSDK\exceptions\AccessException;
use PromodjSDK\exceptions\RequestException;
use PromodjSDK\exceptions\ValidateException;
use PromodjSDK\models\common\TrackShort;
use PromodjSDK\models\musicianTracks\Group;
use PromodjSDK\requests\GetGroupTracksRequest;
use PromodjSDK\requests\GetMusicianDataRequest;
use PromodjSDK\requests\GetMusicianTracksRequest;
use PromodjSDK\requests\GetTrackDataRequest;
use PromodjSDK\requests\MusicSearchRequest;
use PromodjSDK\responses\GetMusicianDataResponse;
use PromodjSDK\responses\GetMusicianTracksResponse;
use PromodjSDK\responses\GetTrackDataResponse;

class TestCasePromodj extends TestCase
{

    /**
     * @var Client|null
     */
    private $client;

    private const TRACK_TEST_DATA_PATH = 'data/track/';
    private const MUSICIAN_TEST_DATA_PATH = 'data/musician/';
    private const MUSICIAN_MUSIC_TEST_DATA_PATH = 'data/musicianMusic/';
    private const GROUP_TEST_DATA_PATH = 'data/group/';

    /**
     * Маппинг страницы трека к json файлу для проверки ответа SDK (Client::getTrackData())
     */
    private const TRACK_ROUTES_TO_JSON_MAP = [
        '/filatovkaras/tracks/7192420/Filatov_Karas_vs_Mumiy_Troll_Amore_More_Goodbye' => 'Filatov_Karas_vs_Mumiy_Troll_Amore_More_Goodbye.json',
        '/djratek/mixes/7184899/DJ_RATEK_VIP_Electro_June_2021' => 'DJ_RATEK_VIP_Electro_June_2021.json',
        '/quba.ua/remixes/4789306/Jason_Derulo_Ft_Snoop_Dogg_Wiggle_Quba_Booty_mix' => 'Jason_Derulo_Ft_Snoop_Dogg_Wiggle_Quba_Booty_mix.json',
        '/jim/mixes/6952899/DJ_JIM_8_bit_Electro_Speed' => 'DJ_JIM_8_bit_Electro_Speed.json',
        '/dinrise/podcasts/6083584/Deep_Session_22_Passion' => 'Deep_Session_22_Passion.json',
        '/filatovkaras/videos/7150424/Filatov_Karas_TechNoNo' => 'Filatov_Karas_TechNoNo.json', //видео
    ];

    /**
     * Маппинг страницы музыканта к json файлу для проверки ответа SDK (Client::getMusicianData())
     */
    private const MUSICIAN_ROUTES_TO_JSON_MAP = [
        '/jim/contact' => 'jim.json',
        '/alex.black/contact' => 'alex.black.json',
        '/djbaur/contact' => 'djbaur.json',
        '/dimixer/contact' => 'dimixer.json',
        '/dj-egorov/contact' => 'dj-egorov.json',
    ];

    /**
     * Маппинг страницы музыки музыканта к json файлу для проверки ответа SDK (Client::getMusicianTracks())
     */
    private const MUSICIAN_MUSIC_TO_JSON_MAP = [
        '/dj-egorov/music' => 'dj-egorov.json',
        '/dj-amor/music' => 'dj-amor.json',
        '/filatovkaras/music' => 'filatovkaras.json',
        '/jim/music' => 'jim.json',
        '/djbaur/music' => 'djbaur.json',
    ];

    /**
     * В ответе Client::getMusicianTracks() много треков и их количество меняется.
     * Этот маппинг нужен, чтобы ограничить ответ метода одним треком, чтобы сверить ответ.
     */
    private const MUSICIAN_MUSIC_TO_TRACK_MAP = [
        '/dj-egorov/music' => 'https://promodj.com/dj-egorov/remixes/7184373/The_Black_Eyed_Peas_Imma_Be_Lebedeff_x_Egorov_x_KOFA_Remix',
        '/dj-amor/music' => 'https://promodj.com/dj-amor/remixes/7120420/Kylie_Minogue_Real_Groove_Amor_Radio_mix',
        '/filatovkaras/music' => 'https://promodj.com/filatovkaras/tracks/7192420/Filatov_Karas_vs_Mumiy_Troll_Amore_More_Goodbye',
        '/jim/music' => 'https://promodj.com/jim/mixes/6952899/DJ_JIM_8_bit_Electro_Speed',
        '/djbaur/music' => 'https://promodj.com/djbaur/mixes/7266237/DJ_BAUR_KLUBBTOOLS_8_0_Mix',
    ];

    /**
     * В ответе Client::getMusicianTracks() много групп и их количество меняется.
     * Этот маппинг нужен, чтобы ограничить ответ метода одной группой, чтобы сверить ответ.
     */
    private const MUSICIAN_MUSIC_TO_GROUP_MAP = [
        '/dj-egorov/music' => 'https://promodj.com/dj-egorov/groups/495850/DJ_SHTOPOR_amp_DJ_OLEG_PETROFF_MOSCOW_CALLING_PODCAST',
        '/dj-amor/music' => 'https://promodj.com/dj-amor/groups/648573/2020',
        '/filatovkaras/music' => 'https://promodj.com/filatovkaras/groups/664995/Filatov_Karas_TechNoNo',
        '/jim/music' => 'https://promodj.com/jim/groups/13900/Electro_Speed_Mixes',
        '/djbaur/music' => 'https://promodj.com/djbaur/groups/659456/https_telegram_me_djbaur',
    ];

    /**
     * Маппинг ссылки на группу (Client::getGroupTracks()) на json файл для проверки ответа SDK.
     */
    private const GROUP_TO_JSON_MAP = [
        '/andivax/tracks' => 'andivax.json',
        '/dj-amor/groups/648573/2020' => '648573.json',
        '/filatovkaras/groups/664995/Filatov_Karas_TechNoNo' => '664995.json', //видео
        '/johnkayder/groups/648462/MEGA_MIX_2020' => '648462.json',
        '/djperetse/groups/480395/Retro_Megamiksi' => '7156911.json',
        '/antonpavlovsky/groups/434109/2014' => '434109.json',
    ];

    /**
     * На странице группы много треков и они сдвигаются при добавлении новых.
     * Этот маппинг нужен, чтобы сократить количество треков до одного и сверить ответ SDK (Client::getGroupTracks())
     * Учитывается только первая страница
     */
    private const GROUP_TO_TRACK_MAP = [
        '/andivax/tracks' => 'https://promodj.com/andivax/tracks/6650993/GrekoRoman_Znak_Okliku',
        '/dj-amor/groups/648573/2020' => 'https://promodj.com/dj-amor/remixes/7049186/Jah_Khalib_Havana_Dj_Amor_Remix',
        '/filatovkaras/groups/664995/Filatov_Karas_TechNoNo' => 'https://promodj.com/filatovkaras/videos/7150424/Filatov_Karas_TechNoNo', //видео
        '/johnkayder/groups/648462/MEGA_MIX_2020' => 'https://promodj.com/johnkayder/podcasts/7023291/John_Kayder_DREAMING_MEGA_MIX_13_06_2020_10',
        '/djperetse/groups/480395/Retro_Megamiksi' => 'https://promodj.com/djperetse/radioshows/7156911/DJ_Peretse_Record_Megamix_80_01_04_2021_2346',
        '/antonpavlovsky/groups/434109/2014' => 'https://promodj.com/antonpavlovsky/remixes/4945683/D_Tavare_vs_S_Rea_Summer_night_on_the_beach_Anton_Pavlovsky_Cover',
    ];

    /**
     * @throws AccessException
     * @throws RequestException
     * @throws ValidateException
     * @throws InvalidSelectorException
     * @throws ValidationException
     */
    public function testGetTrackData(): void
    {
        foreach (self::TRACK_ROUTES_TO_JSON_MAP as $route => $testJsonPath) {
            $request = new GetTrackDataRequest($route);
            /** @var GetTrackDataResponse $response */
            $response = $this->getClient()->getTrackData($request);
            $isSuccess = $response->isSuccess();
            $errors = $response->getErrors();

            $response->common->promorank = 0;
            $response->description->countOfListens = 0;
            $response->description->countOfDownloads = 0;
            $response->description->countOfBookmarks = 0;
            $responseJson = $this->reSerializeJson(json_encode($response));
            $testJson = $this->reSerializeJson(file_get_contents(self::TRACK_TEST_DATA_PATH . $testJsonPath));

            $this->assertTrue($isSuccess && $responseJson === $testJson);
        }
    }

    /**
     * @throws AccessException
     * @throws InvalidSelectorException
     * @throws RequestException
     * @throws ValidateException
     * @throws ValidationException
     */
    public function testGetMusicianData(): void
    {
        foreach (self::MUSICIAN_ROUTES_TO_JSON_MAP as $route => $testJsonPath) {
            $request = new GetMusicianDataRequest($route);
            /** @var GetMusicianDataResponse $response */
            $response = $this->getClient()->getMusicianData($request);
            $isSuccess = $response->isSuccess();
            $errors = $response->getErrors();

            $responseJson = $this->reSerializeJson(json_encode($response));
            $testJson = $this->reSerializeJson(file_get_contents(self::MUSICIAN_TEST_DATA_PATH . $testJsonPath));

            $this->assertTrue($isSuccess && $responseJson === $testJson);
        }
    }

    /**
     * @throws AccessException
     * @throws InvalidSelectorException
     * @throws RequestException
     * @throws ValidateException
     * @throws ValidationException
     */
    public function testGetMusicianTracks(): void
    {
        foreach (self::MUSICIAN_MUSIC_TO_JSON_MAP as $route => $testJsonPath) {
            $request = new GetMusicianTracksRequest($route);
            /** @var GetMusicianTracksResponse $response */
            $response = $this->getClient()->getMusicianTracks($request);
            $isSuccess = $response->isSuccess();
            $errors = $response->getErrors();

            $response->groups = $this->getOneGroup($route, $response->groups, self::MUSICIAN_MUSIC_TO_GROUP_MAP);
            $response->tracks = $this->getOneTrack($route, $response->tracks, self::MUSICIAN_MUSIC_TO_TRACK_MAP);
            $responseJson = $this->reSerializeJson(json_encode($response));
            $testJson = $this->reSerializeJson(file_get_contents(self::MUSICIAN_MUSIC_TEST_DATA_PATH . $testJsonPath));

            $this->assertTrue($isSuccess && $responseJson === $testJson);
        }
    }

    /**
     * @throws AccessException
     * @throws InvalidSelectorException
     * @throws RequestException
     * @throws ValidateException
     * @throws ValidationException
     */
    public function testGetGroupTracks(): void
    {
        foreach (self::GROUP_TO_JSON_MAP as $route => $testJsonPath) {
            $request = new GetGroupTracksRequest($route, 1);
            $response = $this->getClient()->getGroupTracks($request);
            $isSuccess = $response->isSuccess();
            $errors = $response->getErrors();

            $response->tracks = $this->getOneTrack($route, $response->tracks, self::GROUP_TO_TRACK_MAP);
            $responseJson = $this->reSerializeJson(json_encode($response));
            $testJson = $this->reSerializeJson(file_get_contents(self::GROUP_TEST_DATA_PATH . $testJsonPath));

            $this->assertTrue($isSuccess && $responseJson === $testJson);
        }
    }

    /**
     * @throws AccessException
     * @throws InvalidSelectorException
     * @throws RequestException
     * @throws ValidateException
     * @throws ValidationException
     */
    public function testMusicSearch(): void
    {
        $request = (new MusicSearchRequest(Kinds::TRACK, Styles::ELECTROHOUSE))
            ->setAge(30, 50)
            ->setBpm(128)
            ->setDuration(MusicSearchRequest::DURATION_LESS_THEN_10_MINUTES)
            ->setExcludeJunk()
            ->setForMonth(9, 2012);
        $response = $this->getClient()->musicSearch($request);
        $isSuccess = $response->isSuccess();
        $errors = $response->getErrors();
        $responseJson = $this->reSerializeJson(json_encode($response));

        $this->assertTrue($isSuccess);
    }

    /**
     * @param string $musicianMusicRoute
     * @param Group[] $groups
     * @param array $mapper
     * @return Group[]
     */
    private function getOneGroup(string $musicianMusicRoute, array $groups, array $mapper): array
    {
        $groupHref = $mapper[$musicianMusicRoute];
        foreach ($groups as $group) {
            if($group->href == $groupHref) {
                $group->count = 0;
                return [$group];
            }
        }

        return [];
    }

    /**
     * @param string $musicianMusicRoute
     * @param TrackShort[] $tracks
     * @param array $mapper
     * @return TrackShort[]
     */
    private function getOneTrack(string $musicianMusicRoute, array $tracks, array $mapper): array
    {
        $trackHref = $mapper[$musicianMusicRoute];
        foreach ($tracks as $track) {
            if($track->href == $trackHref) {
                $track->countOfListens = 0;
                $track->countOfComments = 0;
                $track->countOfDownloads = 0;
                $track->promorank = 0;
                return [$track];
            }
        }

        return [];
    }

    private function getClient(): Client
    {
        if(!$this->client) {
            $this->client = new Client();
        }

        return $this->client;
    }

    private function reSerializeJson(string $json): string
    {
        return json_encode(json_decode($json));
    }
}