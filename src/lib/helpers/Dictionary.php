<?php

namespace PromodjSDK\helpers;

class Dictionary
{
    public const BASE_URL = 'https://promodj.com';
    public const DEFAULT_DATE_FORMAT = 'Y-m-d H:i:s';
}