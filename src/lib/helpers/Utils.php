<?php

namespace PromodjSDK\helpers;

use DateTime;
use PromodjSDK\enums\Styles;

class Utils
{

    private const TODAY = 'сегодня';
    private const YESTERDAY = 'вчера';

    private const MONTH_NUMBER_MAP = [
        "января" => 1,
        "февраля" => 2,
        "марта" => 3,
        "апреля" => 4,
        "мая" => 5,
        "июня" => 6,
        "июля" => 7,
        "августа" => 8,
        "сентября" => 9,
        "октября" => 10,
        "ноября" => 11,
        "декабря" => 12,
    ];

    public static function getNumbersFromString(?string $string): int
    {
        return intval(preg_replace('/[^\d]/', '', $string) ?: 0);
    }

    public static function getFloatFromString(?string $string): float
    {
        $floatString = preg_replace('/[^\d.,]/', '', $string);
        return floatval($floatString ? str_replace(',', '.', $floatString) : 0);
    }

    /**
     * Преобразует время в формате H:i:s в секунды
     * @param string|null $time
     * @return int
     */
    public static function getSecondsFromTime(?string $time): ?int
    {
        if($time && preg_match('/(\d:\d:\d)|(\d:\d)/', $time)) {
            $seconds = 0;
            $timeUnits = explode(':', $time);
            if(is_array($timeUnits) && count($timeUnits)) {
                $iterator = 0;
                foreach (array_reverse($timeUnits) as $timeUnit) {
                    if($iterator == 0) {
                        $seconds += $timeUnit;
                    }

                    if($iterator == 1) {
                        $seconds += $timeUnit * 60;
                    }

                    if($iterator == 2) {
                        $seconds += $timeUnit * 60 * 60;
                    }

                    $iterator++;
                }
            }

            return $seconds;
        }

        return null;
    }

    /**
     * Преобразует "В эфире PromoDJ FM с 27 июня 2021 " в \DateTime
     *
     * @param string|null $string
     * @return DateTime|null
     */
    public static function getDateFromString(?string $string): ?DateTime
    {
        if($string) {
            if(mb_stripos($string, self::TODAY) !== false) {
                return self::setNullTime(new DateTime());
            }

            if(mb_stripos($string, self::YESTERDAY) !== false) {
                return self::setNullTime(new DateTime('-1 day'));
            }

            if(preg_match('/(\d{1,2} \w{3,} \d{4})/ui', $string, $matches)) {
                $dateString = $matches[0];
                foreach (self::MONTH_NUMBER_MAP as $rusMonthName => $number) {
                    $dateString = str_replace($rusMonthName, $number, $dateString);
                }

                return self::setNullTime(DateTime::createFromFormat("j n Y", $dateString));
            }
        }

        return null;
    }

    private static function setNullTime(DateTime $dateTime): DateTime
    {
        return $dateTime->setTime(0, 0, 0);
    }

    /**
     * Преобразует строки типа:
     * 25 июня 2021 15:22
     * вчера
     * сегодня
     * в \DateTime
     *
     * @param string|null $string
     * @return DateTime|null
     */
    public static function getDateTimeFromString(?string $string): ?DateTime
    {
        if($string) {
            foreach(self::MONTH_NUMBER_MAP as $monthName => $monthNumber) {
                $string = str_replace($monthName, $monthNumber, $string);
            }

            $today = (new DateTime())->format("j n Y");
            $string = str_replace("сегодня", $today, $string);

            $yesterday = (new DateTime('-1 day'))->format("j n Y");
            $string = str_replace("вчера", $yesterday, $string);

            if(strpos($string, ':') === false) {
                $string .= " 0:00";
            }

            if(preg_match('/\d{1,2} \d{1,2} \d{4} \d{1,2}:\d{2}/u', $string, $matches)) {
                $string = $matches[0];
                return DateTime::createFromFormat("j n Y H:i:s", $string . ':00');
            }
        }

        return null;
    }

    /**
     * @param string|null $string
     * @return array
     */
    public static function getStylesFromString(?string $string): array
    {
        $result = [];
        if($string) {
            foreach (Styles::WITH_COMMA_NAME_LIST as $style) {
                if(strpos($string, $style)) {
                    $string = str_replace($style, '', $string);
                    $result[] = $style;
                }
            }

            $styles = explode(',', $string);
            if(is_array($styles) && count($styles)) {
                foreach ($styles as $style) {
                    $style = trim($style);
                    if($style) {
                        $result[] = $style;
                    }
                }
            }
        }

        return $result;
    }
}