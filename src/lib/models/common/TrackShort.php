<?php

namespace PromodjSDK\models\common;

use PromodjSDK\enums\Styles;
use PromodjSDK\models\base\AbstractModel;
use Respect\Validation\Validator as v;

class TrackShort extends AbstractModel
{

    /**
     * Ссылка на страницу трека
     * @var string
     */
    public $href;

    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $countOfListens;

    /**
     * @var int
     */
    public $length;

    /**
     * @var int
     */
    public $countOfComments;

    /**
     * @var string|null
     */
    public $downloadMp3OrMovUrl;

    /**
     * @var int
     */
    public $countOfDownloads;

    /**
     * @var float
     */
    public $promorank;

    /**
     * @var string[]
     */
    public $styles;

    /**
     * @var bool
     */
    public $isVideo;

    public function rules(): array
    {
        return [
            'name' => v::nullable(v::stringVal()->stringType()),
            'countOfListens' => v::nullable(v::intVal()->intType()),
            'length' => v::nullable(v::intVal()->intType()),
            'countOfComments' => v::nullable(v::intVal()->intType()),
            'countOfDownloads' => v::nullable(v::intVal()->intType()),
            'promorank' => v::nullable(v::floatVal()->floatType()),
            'styles' => v::each(v::nullable(v::in(Styles::getStyleNamesWithMusicianTools()))),
            'isVideo' => v::nullable(v::boolVal()->boolType()),
        ];
    }
}