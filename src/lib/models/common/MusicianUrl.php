<?php

namespace PromodjSDK\models\common;

use PromodjSDK\models\base\AbstractModel;
use Respect\Validation\Validator as v;

class MusicianUrl extends AbstractModel
{
    /**
     * @var string
     */
    public $nickname;

    /**
     * @var string
     */
    public $url;

    public function rules(): array
    {
        return [
            'nickname' => v::stringVal()->stringType(),
            'url' => v::url(),
        ];
    }
}