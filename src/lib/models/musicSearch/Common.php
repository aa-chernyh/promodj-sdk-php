<?php

namespace PromodjSDK\models\musicSearch;

use PromodjSDK\models\base\AbstractModel;
use Respect\Validation\Validator as v;

class Common extends AbstractModel
{

    /**
     * @var bool
     */
    public $nextPageAvailable;

    public function rules(): array
    {
        return [
            'nextPageAvailable' => v::boolVal()->boolType(),
        ];
    }
}