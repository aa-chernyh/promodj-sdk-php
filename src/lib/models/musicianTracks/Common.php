<?php

namespace PromodjSDK\models\musicianTracks;

use PromodjSDK\models\base\AbstractModel;
use Respect\Validation\Validator as v;

class Common extends AbstractModel
{

    /**
     * @var int|null
     */
    public $countTracksInShownGroup;

    public function rules(): array
    {
        return [
            'countTracksInShownGroup' => v::nullable(v::intVal()->intType()),
        ];
    }
}