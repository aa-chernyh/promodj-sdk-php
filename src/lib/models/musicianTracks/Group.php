<?php

namespace PromodjSDK\models\musicianTracks;

use PromodjSDK\models\base\AbstractModel;
use Respect\Validation\Validator as v;

class Group extends AbstractModel
{

    /**
     * @var string
     */
    public $href;

    /**
     * @var int
     */
    public $count;

    public function rules(): array
    {
        return [
            'href' => v::nullable(v::url()),
            'count' => v::nullable(v::intVal()->intType()),
        ];
    }
}