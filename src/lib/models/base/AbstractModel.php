<?php

namespace PromodjSDK\models\base;

use PromodjSDK\common\AbstractStruct;
use Respect\Validation\ChainedValidator;
use Respect\Validation\Exceptions\NestedValidationException;

abstract class AbstractModel extends AbstractStruct
{

    /**
     * @var array
     */
    private $errors = [];

    abstract public function rules(): array;

    public function validate(): bool
    {
        /**
         * @var string $attribute
         * @var ChainedValidator $rule
         */
        foreach ($this->rules() as $attribute => $rule) {
            try {
                $rule->assert($this->{$attribute});
            } catch (NestedValidationException $exception) {
                $this->setErrors($attribute, $exception->getMessages());
            }
        }

        return !count($this->errors);
    }

    protected function setErrors(string $attribute, array $errors, ?int $iterator = null): void
    {
        if(is_integer($iterator)) {
            $this->errors[$attribute][$iterator] = $errors;
        } else {
            $this->errors[$attribute] = $errors;
        }
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}