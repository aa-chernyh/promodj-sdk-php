<?php

namespace PromodjSDK\models\musician;

use PromodjSDK\models\base\AbstractModel;
use Respect\Validation\Validator as v;

class Common extends AbstractModel
{

    /**
     * @var string
     */
    public $realName;

    /**
     * @var bool
     */
    public $isInTop100;

    public function rules(): array
    {
        return [
            'realName' => v::nullable(v::stringVal()->stringType()),
            'isInTop100' => v::boolVal()->boolType(),
        ];
    }
}