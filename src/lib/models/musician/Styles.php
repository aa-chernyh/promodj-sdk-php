<?php

namespace PromodjSDK\models\musician;

use PromodjSDK\models\base\AbstractModel;
use Respect\Validation\Validator as v;

class Styles extends AbstractModel
{
    /**
     * @var string
     */
    public $main;

    /**
     * @var string[]
     */
    public $favorite;

    public function rules(): array
    {
        return [
            'main' => v::nullable(v::stringVal()->stringType()),
            'favorite' => v::nullable(v::each(v::in(\PromodjSDK\enums\Styles::getStyleNamesWithMusicianTools()))),
        ];
    }
}