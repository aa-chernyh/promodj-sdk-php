<?php

namespace PromodjSDK\models\track;

use PromodjSDK\models\base\AbstractModel;
use Respect\Validation\Validator as v;

class Sources extends AbstractModel
{

    /**
     * Длительность трека в секундах
     * @var float
     */
    public $length;

    /**
     * Вес трека в байтах
     * @var int
     */
    public $size;

    /**
     * Абсолютная ссылка на скачивание
     * @var string
     */
    public $URL;

    /**
     * Абсолютная ссылка на подложку (картинка на фоне плеера)
     * @var string
     */
    public $waveURL;

    public function rules(): array
    {
        return [
            'length' => v::nullable(v::floatVal()->floatType()),
            'size' => v::nullable(v::intVal()->intType()),
            'URL' => v::nullable(v::url()),
            'waveURL' => v::nullable(v::url()),
        ];
    }
}