<?php

namespace PromodjSDK\models\track;

use PromodjSDK\models\base\AbstractModel;
use Respect\Validation\Validator as v;

class Common extends AbstractModel
{

    /**
     * @var string
     */
    public $name;

    /**
     * @var float
     */
    public $promorank;

    /**
     * @var bool
     * https://promodj.com/filatovkaras/tracks/7192420/Filatov_Karas_vs_Mumiy_Troll_Amore_More_Goodbye
     */
    public $isInTop100 = false;

    /**
     * @var string|null
     */
    public $downloadUrlAroundName;

    /**
     * @var string|null
     */
    public $downloadM3uUrl;

    /**
     * @var bool
     * https://promodj.com/djratek/mixes/7184899/DJ_RATEK_VIP_Electro_June_2021
     */
    public $isOnPdjFM = false;

    /**
     * @var string|null
     */
    public $onPromodjFmFromDate;

    public function rules(): array
    {
        return [
            'name' => v::stringVal()->stringType(),
            'onPromodjFmFromDate' => v::nullable(v::dateTime()),
            'promorank' => v::floatVal(),
            'isInTop100' => v::boolVal()->boolType(),
            'isOnPdjFM' => v::boolVal()->boolType(),
            'downloadUrlAroundName' => v::nullable(v::url()),
            'downloadM3uUrl' => v::nullable(v::url()),
        ];
    }
}