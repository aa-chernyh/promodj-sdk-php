<?php

namespace PromodjSDK\models\track;

use PromodjSDK\models\base\AbstractModel;
use Respect\Validation\Validator as v;

class DownloadAnchor extends AbstractModel
{

    /**
     * mp3, wav и т.д.
     * @var string
     */
    public $format;

    /**
     * Кбит/с
     * @var int
     */
    public $bitrate;

    /**
     * Ссылка на скачивание трека с самым высоким качеством
     * @var string
     */
    public $href;

    public function rules(): array
    {
        return [
            'format' => v::nullable(v::stringVal()->stringType()),
            'bitrate' => v::nullable(v::intVal()->intType()),
            'href' => v::nullable(v::url()),
        ];
    }
}