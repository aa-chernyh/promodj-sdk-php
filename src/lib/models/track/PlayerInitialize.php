<?php

namespace PromodjSDK\models\track;

use PromodjSDK\enums\Kinds;
use PromodjSDK\models\base\AbstractModel;
use Respect\Validation\Validator as v;

class PlayerInitialize extends AbstractModel
{

    /**
     * @var bool
     */
    public $no_preroll;

    /**
     * @var bool
     */
    public $seekAny;

    /**
     * @var Sources[]
     */
    public $sources;

    /**
     * @var int
     */
    public $defaultSource;

    /**
     * @var int
     */
    public $defaultSourceLQ;

    /**
     * Можно ли скачать?
     * @var bool
     */
    public $downloadable;

    /**
     * @var string
     */
    public $downloadURL;

    /**
     * Название
     * @var string
     */
    public $title;

    /**
     * Название в html
     * @var string
     */
    public $title_html;

    /**
     * Страница трека
     * @var string
     */
    public $titleURL;

    /**
     * Тип (трек, микс, ремикс и т.д.)
     * @var string
     */
    public $kind;

    public function rules(): array
    {
        return [
            'no_preroll' => v::nullable(v::boolVal()->boolType()),
            'seekAny' => v::nullable(v::boolVal()->boolType()),
            'downloadable' => v::nullable(v::boolVal()->boolType()),
            'defaultSource' => v::nullable(v::intVal()->intType()),
            'defaultSourceLQ' => v::nullable(v::intVal()->intType()),
            'downloadURL' => v::nullable(v::url()),
            'titleURL' => v::nullable(v::url()),
            'title' => v::nullable(v::stringVal()->stringType()),
            'title_html' => v::nullable(v::stringVal()->stringType()),
            'kind' => v::nullable(v::in(Kinds::LIST)),
        ];
    }

    public function validate(): bool
    {
        $result = parent::validate();

        if(is_array($this->sources) && count($this->sources)) {
            for($i = 0; $i < count($this->sources); $i++) {
                if(!$this->sources[$i]->validate()) {
                    $result = false;
                    $this->setErrors('sources', $this->sources[$i]->getErrors(), $i);
                }
            }
        }

        return $result;
    }
}