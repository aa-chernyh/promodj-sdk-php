<?php

namespace PromodjSDK\models\track;

use PromodjSDK\models\base\AbstractModel;
use Respect\Validation\Validator as v;

class Links extends AbstractModel
{
    /**
     * @var string|null
     */
    public $shortLink;

    /**
     * @var string|null
     */
    public $playerIframe;

    /**
     * @var string|null
     */
    public $miniPlayerIframe;

    public function rules(): array
    {
        return [
            'shortLink' => v::nullable(v::url()),
            'playerIframe' => v::nullable(v::stringVal()->stringType()),
            'miniPlayerIframe' => v::nullable(v::stringVal()->stringType()),
        ];
    }
}