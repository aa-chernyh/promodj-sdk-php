<?php

namespace PromodjSDK\models\track;

use PromodjSDK\enums\Keys;
use PromodjSDK\enums\Styles;
use PromodjSDK\enums\Sources;
use PromodjSDK\models\base\AbstractModel;
use Respect\Validation\Validator as v;

class Description extends AbstractModel
{
    /**
     * @var DownloadAnchor[]
     */
    public $downloadAnchors = [];

    /**
     * @var string[]
     */
    public $styles = [];

    /**
     * Количество прослушиваний
     * @var int
     */
    public $countOfListens;

    /**
     * Количество скачиваний
     * @var int|null
     */
    public $countOfDownloads;

    /**
     * Количество закладок
     * @var int|null
     */
    public $countOfBookmarks;

    /**
     * Участвует в конкурсе
     * https://promodj.com/russiandigital/remixes/7204226/GROSU_Malinovie_gubi_Dream_Travel_remix_2
     * @var bool
     */
    public $isInCompetition = false;

    /**
     * https://promodj.com/chadsound/remixes/7204223/Dzhigan_feat_Egor_Krid_The_Limba_Blago_White_OG_Buda_Timati_Soda_Luv_Guf_Na_chile_Sasha_First_Eugene_Star_Radio_Edit
     * @var string|null
     */
    public $remixOn;

    /**
     * Продолжительность в секундах
     * @var int
     */
    public $length;

    /**
     * BPM
     * @var int|null
     */
    public $bpm;

    /**
     * BPM от
     * @var int
     * https://promodj.com/jim/mixes/6952899/DJ_JIM_8_bit_Electro_Speed
     */
    public $bpmFrom;

    /**
     * BPM до
     * @var int
     * https://promodj.com/jim/mixes/6952899/DJ_JIM_8_bit_Electro_Speed
     */
    public $bpmTo;

    /**
     * Присутствует мастеринг
     * @var bool
     */
    public $isMastered = false;

    /**
     * Подписан?
     * @var bool
     * https://promodj.com/filatovkaras/tracks/7192420/Filatov_Karas_vs_Mumiy_Troll_Amore_More_Goodbye
     */
    public $isSigned = false;

    /**
     * Podsafe?
     * @var bool
     * https://promodj.com/filatovkaras/tracks/7192420/Filatov_Karas_vs_Mumiy_Troll_Amore_More_Goodbye
     */
    public $isPodsafe = false;

    /**
     * Тональность
     * @var string|null
     * https://promodj.com/amid1578/tracks/7204221/Dj_Mix_A_Moment
     */
    public $key;

    /**
     * Вес в Мегабайтах
     * @var float
     */
    public $sizeInMegaBytes;

    /**
     * Источник
     * @var string
     * https://promodj.com/jim/mixes/6952899/DJ_JIM_8_bit_Electro_Speed
     */
    public $source;

    /**
     * Дата записи
     * @var string|null
     * https://promodj.com/jim/mixes/6952899/DJ_JIM_8_bit_Electro_Speed
     */
    public $recordOnDate;

    /**
     * Дата публикации
     * @var string
     */
    public $publicationOnDate;

    public function rules(): array
    {
        return [
            'styles' => v::each(v::nullable(v::in(Styles::getStyleNamesWithMusicianTools()))),
            'source' => v::nullable(v::in(Sources::NAME_LIST)),
            'key' => v::nullable(v::in(Keys::LIST)),
            'countOfListens' => v::nullable(v::intVal()->intType()),
            'countOfDownloads' => v::nullable(v::intVal()->intType()),
            'countOfBookmarks' => v::nullable(v::intVal()->intType()),
            'length' => v::nullable(v::intVal()->intType()),
            'bpm' => v::nullable(v::intVal()->intType()),
            'bpmFrom' => v::nullable(v::intVal()->intType()),
            'bpmTo' => v::nullable(v::intVal()->intType()),
            'isInCompetition' => v::boolVal()->boolType(),
            'isMastered' => v::boolVal()->boolType(),
            'isSigned' => v::boolVal()->boolType(),
            'isPodsafe' => v::boolVal()->boolType(),
            'remixOn' => v::nullable(v::stringVal()->stringType()),
            'recordOnDate' => v::nullable(v::dateTime()),
            'publicationOnDate' => v::dateTime(),
            'sizeInMegaBytes' => v::nullable(v::floatVal()),
        ];
    }
}