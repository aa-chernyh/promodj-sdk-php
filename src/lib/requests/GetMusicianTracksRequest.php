<?php

namespace PromodjSDK\requests;

class GetMusicianTracksRequest extends AbstractRequest
{

    private const REGEX_MUSICIAN_MUSIC_PAGE = '/^\/{1}.{2,}(\/music){1}\/?$/';

    /**
     * like /dj-amor/music or /dj-amor/music/
     * @return string
     */
    protected function getMethodRouteRegex(): string
    {
        return self::REGEX_MUSICIAN_MUSIC_PAGE;
    }
}