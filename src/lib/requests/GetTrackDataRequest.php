<?php

namespace PromodjSDK\requests;

use PromodjSDK\enums\Kinds;

class GetTrackDataRequest extends AbstractRequest
{

    private const REGEX_TRACK_PAGE_TEMPLATE = '/^\/{1}[^\/]+\/{1}(%s){1}\/{1}\d{2,}\/[^\/]+\/?$/';

    /**
     * like /djrik-rude/remixes/2506513/Scotty_Pirates_Of_The_Caribbean_DJ_RIK_RuDe_Remix
     * or /djrik-rude/remixes/2506513/Scotty_Pirates_Of_The_Caribbean_DJ_RIK_RuDe_Remix/
     * @return string
     */
    protected function getMethodRouteRegex(): string
    {
        return sprintf(self::REGEX_TRACK_PAGE_TEMPLATE, implode('|', Kinds::getSections()));
    }
}