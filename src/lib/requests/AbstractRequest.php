<?php

namespace PromodjSDK\requests;

use ClientInterface\Request;
use PromodjSDK\common\AbstractStruct;
use PromodjSDK\helpers\Dictionary;

abstract class AbstractRequest extends AbstractStruct implements Request
{

    private const FIELD_ROUTE = 'route';

    /**
     * @var string
     */
    public $route;

    /**
     * @var array[]
     */
    private $errors = [];

    public function __construct(?string $route = null)
    {
        $this->route = $route;
    }

    public function validate(): bool
    {
        $this->route = str_replace(Dictionary::BASE_URL, '', $this->route);

        if(!is_string($this->route)) {
            $this->addError(self::FIELD_ROUTE, 'Route не является строкой');
        }

        if(strpos($this->route, '/') === false) {
            $this->addError(self::FIELD_ROUTE, 'Route не содержит начинающий символ /');
        }

        $this->validateMethodRoute();

        return empty($this->errors);
    }

    private function addError(string $fieldName, string $message): void
    {
        $this->errors[$fieldName][] = $message;
    }

    public function getFirstErrors(): array
    {
        $result = [];

        foreach ($this->errors as $attribute => $errors) {
            $result[$attribute] = isset($errors[$attribute][0]) ? $errors[$attribute][0] : '';
        }

        return $result;
    }

    public function getErrors($attribute = null): array
    {
        if($attribute) {
            return isset($this->errors[$attribute]) ? $this->errors[$attribute] : [];
        }
        return $this->errors;
    }

    private function validateMethodRoute(): void
    {
        if(!preg_match($this->getMethodRouteRegex(), $this->route)) {
            $this->addError(self::FIELD_ROUTE, 'Url не соответствует запросу для данного метода');
        }
    }

    abstract protected function getMethodRouteRegex(): string;

    public function buildUrlParams(): void
    {
        $attribures = $this->getAttributes();
        unset($attribures[self::FIELD_ROUTE]);

        if(count($attribures)) {
            $this->route .= '?' . http_build_query($attribures);
        }
    }
}