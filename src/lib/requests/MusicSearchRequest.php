<?php

namespace PromodjSDK\requests;

use PromodjSDK\enums\Kinds;
use PromodjSDK\exceptions\LogicException;

class MusicSearchRequest extends AbstractRequest
{

    private const REGEX_MUSIC_SEARCH = '/\/.*/';

    public const SORT_BY_FRESH = '';
    public const SORT_BY_RATING = 'rating';
    public const SORT_BY_COMMENTS = 'comments';

    private const PERIOD_LAST = 'last';
    private const PERIOD_DATE = 'date';

    public const DURATION_ANY = '';
    public const DURATION_LESS_THEN_1_MINUTE = '1m';
    public const DURATION_LESS_THEN_10_MINUTES = '10m';
    public const DURATION_LESS_THEN_30_MINUTES = '30m';
    public const DURATION_LESS_THEN_60_MINUTES = '60m';
    public const DURATION_MORE_THEN_HOUR = '1h';

    public const QUALITY_ANY = '';
    public const QUALITY_HIGH = 'high';
    public const QUALITY_LOSSLESS = 'lossless';

    /**
     * Сортировка по (константа)
     * @var string|null
     */
    public $sortby;

    /**
     * Тип периода (константа
     * @var string|null
     */
    public $period;

    /**
     * За последние (1d|2w|3m|4y) - (1 день | 2 недели | 3 месяца | 4 года)
     * @var string|null
     */
    public $period_last;

    /**
     * За какой месяц
     * @var int|null
     */
    public $month;

    /**
     * За какой год
     * @var int|null
     */
    public $year;

    /**
     * Продолжительность не более (константа)
     * @var string|null
     */
    public $duration;

    /**
     * Качество (константа)
     * @var string|null
     */
    public $bitrate;

    /**
     * В топ 100?
     * @var int|null
     */
    public $top;

    /**
     * Есть треклист?
     * @var int|null
     */
    public $tracklist;

    /**
     * В ротации на радио?
     * @var int|null
     */
    public $radio;

    /**
     * Продается?
     * @var int|null
     */
    public $sell;

    /**
     * Подписан?
     * @var int|null
     */
    public $release;

    /**
     * Есть рецензия?
     * @var int|null
     */
    public $opinion;

    /**
     * Конкурсная работа?
     * @var int|null
     */
    public $contest;

    /**
     * Присутствует мастеринг?
     * @var int|null
     */
    public $mastering;

    /**
     * Можно скачать?
     * @var int|null
     */
    public $download;

    /**
     * Podsafe-трек?
     * @var int|null
     */
    public $podsafe;

    /**
     * Брендирован?
     * @var int|null
     */
    public $audio_branded;

    /**
     * Включает поиск по BPM
     * @var int|null
     */
    public $bpm_cb;

    /**
     * BPM или BPM от
     * @var int|null
     */
    public $bpm;

    /**
     * BPM до
     * @var int|null
     */
    public $bpm_till;

    /**
     * Тональность
     * @var string|null
     */
    public $tone;

    /**
     * Исключить мэшапы, бутлеги и демо?
     * @var int|null
     */
    public $no_junk;

    /**
     * Настроение жесть
     * @var int|null
     */
    public $its_hard;

    /**
     * Настроение позитив
     * @var int|null
     */
    public $its_positive;

    /**
     * Настроение сон
     * @var int|null
     */
    public $for_sleeping;

    /**
     * Настроение работа
     * @var int|null
     */
    public $for_working;

    /**
     * Настроение ночь
     * @var int|null
     */
    public $for_sunrise;

    /**
     * Пол мальчик
     * @var int|null
     */
    public $sex_man;

    /**
     * Пол девочка
     * @var int|null
     */
    public $sex_woman;

    /**
     * Включает поиск по возрасту
     * @var int|null
     */
    public $age_cb;

    /**
     * Возраст или возраст от
     * @var int|null
     */
    public $age;

    /**
     * Возраст до
     * @var int|null
     */
    public $age_till;

    /**
     * Номер страницы (20 элементов на странице)
     * @var int|null
     */
    public $page;

    /**
     * MusicSearchRequest constructor.
     * @param string|null $kind
     * @param string|null $style
     * @throws LogicException
     */
    public function __construct(?string $kind = null, ?string $style = null)
    {
        $route = '/' . ($kind ? Kinds::getMultipleKind($kind) : 'music');

        if($style) {
            $route .= '/' . $style;
        }

        parent::__construct($route);
    }

    /**
     * Сортировка по (константа)
     * @param string $sortby
     * @return $this
     */
    public function setSortBy(string $sortby): self
    {
        $this->sortby = $sortby;
        return $this;
    }

    /**
     * За последние (1d|2w|3m|4y) - (1 день | 2 недели | 3 месяца | 4 года)
     * @param string $periodLast
     * @return $this
     */
    public function setPeriodLast(string $periodLast): self
    {
        $this->period = self::PERIOD_LAST;
        $this->period_last = $periodLast;
        return $this;
    }

    /**
     * За какой месяц
     * @param int $month
     * @param int $year
     * @return $this
     */
    public function setForMonth(int $month, int $year): self
    {
        $this->period = self::PERIOD_DATE;
        $this->month = $month;
        $this->year = $year;
        return $this;
    }

    /**
     * Продолжительность не более (константа)
     * @param string $duration
     * @return $this
     */
    public function setDuration(string $duration): self
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * Качество (константа)
     * @param string $quality
     * @return $this
     */
    public function setQualiity(string $quality): self
    {
        $this->bitrate = $quality;
        return $this;
    }

    /**
     * В топ 100?
     * @return $this
     */
    public function setIsInTop100(): self
    {
        $this->top = 1;
        return $this;
    }

    /**
     * Есть треклист?
     * @return $this
     */
    public function setHasTracklist(): self
    {
        $this->tracklist = 1;
        return $this;
    }

    /**
     * В ротации на радио?
     * @return $this
     */
    public function setIsOnRadio(): self
    {
        $this->radio = 1;
        return $this;
    }

    /**
     * Продается?
     * @return $this
     */
    public function setIsSelling(): self
    {
        $this->sell = 1;
        return $this;
    }

    /**
     * Подписан?
     * @return $this
     */
    public function setIsSigned(): self
    {
        $this->release = 1;
        return $this;
    }

    /**
     * Есть рецензия?
     * @return $this
     */
    public function setHasOpinion(): self
    {
        $this->opinion = 1;
        return $this;
    }

    /**
     * Конкурсная работа?
     * @return $this
     */
    public function setIsInCompetition(): self
    {
        $this->contest = 1;
        return $this;
    }

    /**
     * Присутствует мастеринг?
     * @return $this
     */
    public function setIsMastered(): self
    {
        $this->mastering = 1;
        return $this;
    }

    /**
     * Можно скачать?
     * @return $this
     */
    public function setIsDownloadable(): self
    {
        $this->download = 1;
        return $this;
    }

    /**
     * Podsafe-трек
     * @return $this
     */
    public function setIsPodsafe(): self
    {
        $this->podsafe = 1;
        return $this;
    }

    /**
     * Брендирован?
     * @return $this
     */
    public function setIsBranded(): self
    {
        $this->audio_branded = 1;
        return $this;
    }

    /**
     * Диапазон BPM или конкретный BPM
     * @param int $bpm
     * @param int|null $bpmTo
     * @return $this
     */
    public function setBpm(int $bpm, ?int $bpmTo = null): self
    {
        $this->bpm_cb = 1;
        $this->bpm = $bpm;
        $this->bpm_till = $bpmTo;
        return $this;
    }

    /**
     * Тональность (константа enums\Keys)
     * @param string $key
     * @return $this
     */
    public function setKey(string $key): self
    {
        $this->tone = $key;
        return $this;
    }

    /**
     * Исключить мэшапы, бутлеги и демо?
     * @return $this
     */
    public function setExcludeJunk(): self
    {
        $this->no_junk = 1;
        return $this;
    }

    /**
     * Настроение жесть
     * @return $this
     */
    public function setMoodHard(): self
    {
        $this->its_hard = 1;
        return $this;
    }

    /**
     * Настроение позитив
     * @return $this
     */
    public function setMoodPositive(): self
    {
        $this->its_positive = 1;
        return $this;
    }

    /**
     * Настроение сон
     * @return $this
     */
    public function setMoodSleep(): self
    {
        $this->for_sleeping = 1;
        return $this;
    }

    /**
     * Настроение работа
     * @return $this
     */
    public function setMoodWork(): self
    {
        $this->for_working = 1;
        return $this;
    }

    /**
     * Настроение ночь
     * @return $this
     */
    public function setMoodNight(): self
    {
        $this->for_sunrise = 1;
        return $this;
    }

    /**
     * Пол мальчик
     * @return $this
     */
    public function setSexMan(): self
    {
        $this->sex_man = 1;
        return $this;
    }

    /**
     * Пол девочка
     * @return $this
     */
    public function setSexWoman(): self
    {
        $this->sex_woman = 1;
        return $this;
    }

    /**
     * Возраст автора или диапазон возрастов
     * @param int $age
     * @param int|null $ageTo
     * @return $this
     */
    public function setAge(int $age, ?int $ageTo = null): self
    {
        $this->age_cb = 1;
        $this->age = $age;
        $this->age_till = $ageTo;
        return $this;
    }

    public function setPage(int $page): self
    {
        $this->page = $page;
        return $this;
    }

    protected function getMethodRouteRegex(): string
    {
        return self::REGEX_MUSIC_SEARCH;
    }
}