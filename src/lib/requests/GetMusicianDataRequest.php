<?php

namespace PromodjSDK\requests;

class GetMusicianDataRequest extends AbstractRequest
{

    private const REGEX_MUSICIAN_DATA_PAGE = '/^\/[^\/]*\/contact\/?$/';

    /**
     * like /dj-amor/contact or /dj-amor/contact/
     * @return string
     */
    protected function getMethodRouteRegex(): string
    {
        return self::REGEX_MUSICIAN_DATA_PAGE;
    }
}