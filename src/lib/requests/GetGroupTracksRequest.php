<?php

namespace PromodjSDK\requests;

class GetGroupTracksRequest extends AbstractRequest
{

    /**
     * @var int|null
     */
    public $page;

    private const REGEX_GROUP_TRACKS_PAGE = '/^\/[^\/]+\/[^?]+\/?(\?page=(\d)+)?$/';

    /**
     * like /romapafos/groups/2201/DJ_SET_Electro_House_Progressive_House
     * or /romapafos/groups/2201/DJ_SET_Electro_House_Progressive_House/
     * or /romapafos/groups/2201/DJ_SET_Electro_House_Progressive_House?page=2
     * or /romapafos/groups/2201/DJ_SET_Electro_House_Progressive_House/?page=2
     * @return string
     */
    protected function getMethodRouteRegex(): string
    {
        return self::REGEX_GROUP_TRACKS_PAGE;
    }

    public function __construct(?string $route = null, ?int $page = null)
    {
        parent::__construct($route);
        $this->page = $page;
    }
}