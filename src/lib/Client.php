<?php

namespace PromodjSDK;

use ClientInterface\Exception\ValidationException;
use DiDom\Exceptions\InvalidSelectorException;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use PromodjSDK\analyzers\GroupAnalyzer;
use PromodjSDK\analyzers\MusicianAnalyzer;
use PromodjSDK\analyzers\MusicianTracksAnalyzer;
use PromodjSDK\analyzers\MusicSearchAnalyzer;
use PromodjSDK\analyzers\TrackAnalyzer;
use PromodjSDK\exceptions\AccessException;
use PromodjSDK\exceptions\RequestException;
use PromodjSDK\exceptions\ValidateException;
use PromodjSDK\helpers\Dictionary;
use PromodjSDK\requests\AbstractRequest;
use PromodjSDK\requests\GetGroupTracksRequest;
use PromodjSDK\requests\GetMusicianDataRequest;
use PromodjSDK\requests\GetMusicianTracksRequest;
use PromodjSDK\requests\GetTrackDataRequest;
use PromodjSDK\requests\MusicSearchRequest;
use PromodjSDK\responses\AbstractResponse;
use PromodjSDK\responses\GetGroupTracksResponse;
use PromodjSDK\responses\GetMusicianDataResponse;
use PromodjSDK\responses\GetMusicianTracksResponse;
use PromodjSDK\responses\GetTrackDataResponse;
use PromodjSDK\responses\MusicSearchResponse;

class Client
{

    private const HTTP_TIMEOUT = 60;
    private const HTTP_METHOD_GET = 'GET';
    private const HTTP_STATUS_SUCCESS = 200;

    /**
     * @var array
     */
    protected $lastRequest = [];

    /**
     * @var AbstractResponse
     */
    protected $lastResponse;

    /**
     * @var string
     */
    protected $lastErrorMessage = '';

    /**
     * Последний URL, на который был сделан запрос
     * @var string
     */
    protected $lastUrl = '';

    /**
     * Заголовки последнего запроса
     * @var array
     */
    protected $lastRequestHeaders = [];

    /**
     * Заголовки последнего ответа
     * @var array
     */
    protected $lastResponseHeaders = [];

    /**
     * Строка вида login:password@ip:port
     * @var string|null
     */
    private $proxy;

    public function __construct(?string $proxy = null)
    {
        $this->proxy = $proxy;
    }

    public function getLastRequest(): array
    {
        return $this->lastRequest ?? [];
    }

    public function getLastResponse(): array
    {
        return $this->lastResponse ? $this->lastResponse->toArray() : [];
    }

    public function getLastResult(): bool
    {
        return !$this->lastErrorMessage;
    }

    public function getLastErrorMessage(): string
    {
        return $this->lastErrorMessage;
    }

    public function getLastUrl(): string
    {
        return $this->lastUrl;
    }

    public function getLastRequestHeaders(): array
    {
        return $this->lastRequestHeaders ?? [];
    }

    public function getLastResponseHeaders(): array
    {
        return $this->lastResponseHeaders ?? [];
    }

    /**
     * @param GetTrackDataRequest $request
     * @return GetTrackDataResponse
     * @throws AccessException
     * @throws RequestException
     * @throws ValidateException
     * @throws InvalidSelectorException
     * @throws ValidationException
     */
    public function getTrackData(GetTrackDataRequest $request): GetTrackDataResponse
    {
        $html = $this->makeRequest($request);
        $analyzer = new TrackAnalyzer($html);
        $this->lastResponse = $analyzer->service();
        return $this->lastResponse;
    }

    /**
     * @param GetMusicianDataRequest $request
     * @return GetMusicianDataResponse
     * @throws AccessException
     * @throws RequestException
     * @throws ValidateException
     * @throws ValidationException
     * @throws InvalidSelectorException
     */
    public function getMusicianData(GetMusicianDataRequest $request): GetMusicianDataResponse
    {
        $html = $this->makeRequest($request);
        $analyzer = new MusicianAnalyzer($html);
        $this->lastResponse = $analyzer->service();
        return $this->lastResponse;
    }

    /**
     * @param GetMusicianTracksRequest $request
     * @return GetMusicianTracksResponse
     * @throws AccessException
     * @throws RequestException
     * @throws ValidateException
     * @throws ValidationException
     * @throws InvalidSelectorException
     */
    public function getMusicianTracks(GetMusicianTracksRequest $request): GetMusicianTracksResponse
    {
        $html = $this->makeRequest($request);
        $analyzer = new MusicianTracksAnalyzer($html);
        $this->lastResponse = $analyzer->service();
        return $this->lastResponse;
    }

    /**
     * @param GetGroupTracksRequest $request
     * @return GetGroupTracksResponse
     * @throws AccessException
     * @throws InvalidSelectorException
     * @throws RequestException
     * @throws ValidateException
     * @throws ValidationException
     */
    public function getGroupTracks(GetGroupTracksRequest $request): GetGroupTracksResponse
    {
        $html = $this->makeRequest($request);
        $analyzer = new GroupAnalyzer($html);
        $this->lastResponse = $analyzer->service();
        return $this->lastResponse;
    }

    /**
     * @param MusicSearchRequest $request
     * @return MusicSearchResponse
     * @throws AccessException
     * @throws InvalidSelectorException
     * @throws RequestException
     * @throws ValidateException
     * @throws ValidationException
     */
    public function musicSearch(MusicSearchRequest $request): MusicSearchResponse
    {
        $html = $this->makeRequest($request);
        $analyzer = new MusicSearchAnalyzer($html);
        $this->lastResponse = $analyzer->service();
        return $this->lastResponse;
    }

    /**
     * @param $request AbstractRequest
     * @return string html
     * @throws AccessException
     * @throws RequestException
     * @throws ValidateException
     * @throws ValidationException
     */
    private function makeRequest(AbstractRequest $request): string
    {
        $request->buildUrlParams();

        if (!$request->validate()) {
            $this->lastErrorMessage =
                'Валидация запроса прошла неуспешно: '
                . json_encode($request->getErrors())
                . ' аттрибуты: '
                . json_encode($request->getAttributes());
            throw new ValidateException($this->lastErrorMessage);
        }

        $this->lastUrl = Dictionary::BASE_URL . $request->route;
        $this->lastRequest = $request->toArray();
        $this->lastRequestHeaders = [
            'Accept-Language' => 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
        ];

        $clientParams = [
            'base_uri' => Dictionary::BASE_URL,
            'timeout' => self::HTTP_TIMEOUT,
            'headers' => $this->lastRequestHeaders,
        ];
        if($this->proxy) {
            $clientParams['proxy'] = $this->proxy;
        }
        $httpClient = new HttpClient($clientParams);

        try {
            $result = $httpClient->request(self::HTTP_METHOD_GET, $request->route);
            if ($result->getStatusCode() != self::HTTP_STATUS_SUCCESS) {
                $this->lastErrorMessage = "Неверный статус ответа: {$result->getStatusCode()}";
                throw new AccessException($this->lastErrorMessage);
            }

            $this->lastResponseHeaders = $result->getHeaders();
            $html = $result->getBody()->getContents();

            if (!$html) {
                $this->lastErrorMessage = 'Нет ответа';
                throw new AccessException($this->lastErrorMessage);
            }

            if (strpos($html, "charset=utf-8") === false) {
                $html = str_replace('</head>', '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>', $html);
            }

            return $html;
        } catch (GuzzleException $exception) {
            $this->lastErrorMessage = $exception->getMessage();
            throw new RequestException($this->lastErrorMessage);
        }
    }
}