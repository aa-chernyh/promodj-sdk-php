<?php

namespace PromodjSDK\analyzers;

use DiDom\Exceptions\InvalidSelectorException;
use PromodjSDK\analyzers\base\AggregateAnalyzer;
use PromodjSDK\analyzers\common\PlayerStandardAnalyzer;
use PromodjSDK\analyzers\musicSearch\CommonAnalyzer;
use PromodjSDK\responses\MusicSearchResponse;

class MusicSearchAnalyzer extends AggregateAnalyzer
{

    private const SELECTOR_PLAYERS = '#content > div.tracks_dump > div.player_standard';

    /**
     * @var CommonAnalyzer
     */
    private $commonAnalyzer;

    /**
     * @var PlayerStandardAnalyzer
     */
    private $playerStandardAnalyzer;

    protected function initDependencies(): void
    {
        $this->commonAnalyzer = new CommonAnalyzer($this->document);
        $this->playerStandardAnalyzer = new PlayerStandardAnalyzer($this->document, self::SELECTOR_PLAYERS);
    }

    /**
     * @return MusicSearchResponse
     * @throws InvalidSelectorException
     */
    public function service(): MusicSearchResponse
    {
        $response = new MusicSearchResponse();
        $response->common = $this->commonAnalyzer->service();
        $response->tracks = $this->playerStandardAnalyzer->service();

        return $response;
    }
}