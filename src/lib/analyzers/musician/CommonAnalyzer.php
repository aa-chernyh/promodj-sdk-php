<?php

namespace PromodjSDK\analyzers\musician;

use DiDom\Exceptions\InvalidSelectorException;
use PromodjSDK\analyzers\base\Analyzer;
use PromodjSDK\models\base\AbstractModel;
use PromodjSDK\models\musician\Common;

class CommonAnalyzer extends Analyzer
{

    private const SELECTOR_REAL_NAME = '#content_left div.dj_contacts > div > b';
    private const SELECTOR_IS_IN_TOP_100 = '#content_left div.dj_contacts > div.dj_bit > a > img';

    protected function analyze(): void
    {

    }

    /**
     * @return AbstractModel
     * @throws InvalidSelectorException
     */
    protected function map(): AbstractModel
    {
        $response = new Common();
        $response->realName = $this->getRealName();
        $response->isInTop100 = $this->getIsInTop100();

        return $response;
    }

    /**
     * @return string|null
     * @throws InvalidSelectorException
     */
    private function getRealName(): ?string
    {
        $realNameElement = $this->document->first(self::SELECTOR_REAL_NAME);
        if($realNameElement) {
            $realName = trim($realNameElement->text());
            if(strpos($realName, ':') === false) {
                return $realName;
            }
        }

        return null;
    }

    /**
     * @return bool
     * @throws InvalidSelectorException
     */
    private function getIsInTop100(): bool
    {
        $elements = $this->document->find(self::SELECTOR_IS_IN_TOP_100);
        if(is_array($elements) && count($elements)) {
            foreach ($elements as $element) {
                if($element->attr('src') == 'https://cdn.promodj.com/legacy/i/top100_logo_big.png') {
                    return true;
                }
            }
        }
        return false;
    }
}