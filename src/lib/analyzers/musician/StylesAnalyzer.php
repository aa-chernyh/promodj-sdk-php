<?php

namespace PromodjSDK\analyzers\musician;

use DiDom\Element;
use DiDom\Exceptions\InvalidSelectorException;
use DOMElement;
use PromodjSDK\analyzers\base\Analyzer;
use PromodjSDK\models\base\AbstractModel;
use PromodjSDK\models\musician\Styles;

class StylesAnalyzer extends Analyzer
{

    private const SELECTOR_STYLES = '#content_left div.dj_contacts > div.dj_bit';

    /**
     * @var Element|DOMElement|null
     */
    private $stylesElement;

    /**
     * @throws InvalidSelectorException
     */
    protected function analyze(): void
    {
        $this->analyzeStyles();
    }

    /**
     * @throws InvalidSelectorException
     */
    private function analyzeStyles(): void
    {
        $djBitElements = $this->document->find(self::SELECTOR_STYLES);
        if(is_array($djBitElements) && count($djBitElements)) {
            foreach ($djBitElements as $djBitElement) {
                if(
                    stripos($djBitElement->text(), 'Основной стиль:') !== false
                    || stripos($djBitElement->text(), 'Любимые стили:') !== false
                ) {
                    $this->stylesElement = $djBitElement;
                    return;
                }
            }
        }
    }

    /**
     * @return AbstractModel
     * @throws InvalidSelectorException
     */
    protected function map(): AbstractModel
    {
        $response = new Styles();
        $response->main = $this->getMain();
        $response->favorite = $this->getFavorite();

        return $response;
    }

    /**
     * @return string|null
     * @throws InvalidSelectorException
     */
    private function getMain(): ?string
    {
        if($this->stylesElement && stripos($this->stylesElement->text(), 'Основной стиль:') !== false) {
            $mainStyleElement = $this->stylesElement->first('span.styles');
            if($mainStyleElement) {
                return trim($mainStyleElement->text());
            }
        }

        return null;
    }

    /**
     * @return string[]|null
     * @throws InvalidSelectorException
     */
    private function getFavorite(): ?array
    {
        if($this->stylesElement && stripos($this->stylesElement->text(), 'Любимые стили:') !== false) {
            $searchElements = $this->stylesElement->find('span.styles');
            if(is_array($searchElements) && count($searchElements)) {
                $stylesElementPosition = stripos($this->stylesElement->text(), 'Основной стиль:') !== false ? 1 : 0;
                $favoriteStylesText = $searchElements[$stylesElementPosition]->text();
                $styles = explode(', ', $favoriteStylesText);
                if(is_array($styles) && count($styles)) {
                    return $styles;
                }
            }
        }

        return null;
    }
}