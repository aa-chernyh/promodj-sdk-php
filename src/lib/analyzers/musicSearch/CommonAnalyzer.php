<?php

namespace PromodjSDK\analyzers\musicSearch;

use DiDom\Exceptions\InvalidSelectorException;
use PromodjSDK\analyzers\base\Analyzer;
use PromodjSDK\models\base\AbstractModel;
use PromodjSDK\models\musicSearch\Common;

class CommonAnalyzer extends Analyzer
{

    private const SELECTOR_NEXT_PAGE = 'a#next_page';

    protected function analyze(): void
    {

    }

    /**
     * @return AbstractModel
     * @throws InvalidSelectorException
     */
    protected function map(): AbstractModel
    {
        $response = new Common();
        $response->nextPageAvailable = $this->getIsNextPageAvailable();

        return $response;
    }

    /**
     * @return bool
     * @throws InvalidSelectorException
     */
    private function getIsNextPageAvailable(): bool
    {
        return boolval($this->document->first(self::SELECTOR_NEXT_PAGE));
    }
}