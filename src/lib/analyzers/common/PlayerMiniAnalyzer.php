<?php

namespace PromodjSDK\analyzers\common;

use DiDom\Element;
use DiDom\Exceptions\InvalidSelectorException;
use DOMElement;
use PromodjSDK\analyzers\base\PlayerAnalyzer;
use PromodjSDK\helpers\Utils;

class PlayerMiniAnalyzer extends PlayerAnalyzer
{

    private const SELECTOR_TRACK = '#content_left div.dj_content div.dj_universal > div.dj_universal > div.tracks_dump > div.player_mini';

    protected function getTrackWrappersSelector(): string
    {
        return self::SELECTOR_TRACK;
    }

    /**
     * @param Element|DOMElement|null $element
     * @return string|null
     * @throws InvalidSelectorException
     */
    protected function getHref($element): ?string
    {
        if($element) {
            $titleAnchor = $element->first('div.player_mini_title a');
            if($titleAnchor) {
                return $titleAnchor->attr('href');
            }
        }

        return null;
    }

    /**
     * @param Element|DOMElement|null $element
     * @return string|null
     * @throws InvalidSelectorException
     */
    protected function getName($element): ?string
    {
        if($element) {
            $titleAnchor = $element->first('div.player_mini_title a');
            if($titleAnchor) {
                return trim($titleAnchor->text());
            }
        }

        return null;
    }

    /**
     * @param Element|DOMElement|null $element
     * @return int|null
     * @throws InvalidSelectorException
     */
    protected function getCountOfListens($element): ?int
    {
        if($element) {
            $countOfListensElement = $element->first('div.player_mini_tools__1 > a.player_mini_tool__play');
            if($countOfListensElement) {
                return Utils::getNumbersFromString($countOfListensElement->text());
            }
        }

        return null;
    }

    /**
     * @param Element|DOMElement|null $element
     * @return int|null
     * @throws InvalidSelectorException
     */
    protected function getLength($element): ?int
    {
        if($element) {
            $lengthElement = $element->first('div.player_mini_tools__1 > span.player_mini_tool__time');
            if($lengthElement) {
                $timeString = trim($lengthElement->text());
                return Utils::getSecondsFromTime($timeString);
            }
        }

        return null;
    }

    /**
     * @param Element|DOMElement|null $element
     * @return int|null
     * @throws InvalidSelectorException
     */
    protected function getCountOfComments($element): ?int
    {
        if($element) {
            $countOfCommentsElement = $element->first('div.player_mini_tools__1 > a.player_mini_tool__comments > span');
            if($countOfCommentsElement) {
                return Utils::getNumbersFromString($countOfCommentsElement->text());
            }
        }

        return null;
    }

    /**
     * @param Element|DOMElement|null $element
     * @return string|null
     * @throws InvalidSelectorException
     */
    protected function getDownloadMp3OrMovUrl($element): ?string
    {
        if($element) {
            $countOfDownloadsElement = $element->first('div.player_mini_tools__1 > a.player_mini_tool__downloads');
            if($countOfDownloadsElement) {
                return trim($countOfDownloadsElement->attr('href'));
            }
        }

        return null;
    }

    /**
     * @param Element|DOMElement|null $element
     * @return int|null
     * @throws InvalidSelectorException
     */
    protected function getCountOfDownloads($element): ?int
    {
        if($element) {
            $countOfDownloadsElement = $element->first('div.player_mini_tools__1 > a.player_mini_tool__downloads');
            if($countOfDownloadsElement) {
                return Utils::getNumbersFromString($countOfDownloadsElement->text());
            }
        }

        return null;
    }

    /**
     * @param Element|DOMElement|null $element
     * @return float|null
     * @throws InvalidSelectorException
     */
    protected function getPromorank($element): ?float
    {
        if($element) {
            $promorankElement = $element->first('div.player_mini_tools__1 > span.player_mini_tool__pr > b');
            if($promorankElement) {
                return Utils::getFloatFromString($promorankElement->text());
            }
        }

        return null;
    }

    /**
     * @param Element|DOMElement|null $element
     * @return string[]|null
     * @throws InvalidSelectorException
     */
    protected function getStyles($element): ?array
    {
        if($element) {
            $stylesElement = $element->first('div.player_mini_tools__2 > div.player_mini_tool__styles');
            if($stylesElement) {
                return Utils::getStylesFromString($stylesElement->text());
            }
        }

        return null;
    }

    /**
     * @param Element|DOMElement|null $element
     * @return bool
     */
    protected function getIsVideo($element): bool
    {
        return false;
    }
}