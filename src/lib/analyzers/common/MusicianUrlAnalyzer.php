<?php

namespace PromodjSDK\analyzers\common;

use DiDom\Element;
use DiDom\Exceptions\InvalidSelectorException;
use DOMElement;
use PromodjSDK\analyzers\base\Analyzer;
use PromodjSDK\models\base\AbstractModel;
use PromodjSDK\models\common\MusicianUrl;

class MusicianUrlAnalyzer extends Analyzer
{

    private const SELECTOR_MUSICIAN_NICK_NAME = '#dj_menu .dj_menu_title a.user';

    /**
     * @var Element|DOMElement|null
     */
    private $nameElement;

    protected function analyze(): void
    {
        $this->analyzeMusicianNameElement();
    }

    /**
     * @throws InvalidSelectorException
     */
    private function analyzeMusicianNameElement(): void
    {
        $this->nameElement = $this->document->first(self::SELECTOR_MUSICIAN_NICK_NAME);
    }

    protected function map(): AbstractModel
    {
        $response = new MusicianUrl();
        $response->nickname = $this->getNickName();
        $response->url = $this->getUrl();

        return $response;
    }

    /**
     * @return string|null
     */
    private function getNickName(): ?string
    {
        if(!is_null($this->nameElement)) {
            $nameWithTags = $this->nameElement->text();
            return str_replace(['<i>', '</i>'], '', $nameWithTags);
        }

        return null;
    }

    /**
     * @return string|null
     */
    private function getUrl(): ?string
    {
        if(!is_null($this->nameElement)) {
            return $this->nameElement->attr('href');
        }

        return null;
    }
}