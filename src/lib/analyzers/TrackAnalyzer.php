<?php

namespace PromodjSDK\analyzers;

use ClientInterface\Response;
use DiDom\Exceptions\InvalidSelectorException;
use PromodjSDK\analyzers\base\AggregateAnalyzer;
use PromodjSDK\analyzers\track\DescriptionAnalyzer;
use PromodjSDK\analyzers\track\LinksAnalyzer;
use PromodjSDK\analyzers\common\MusicianUrlAnalyzer;
use PromodjSDK\analyzers\track\PlayerInitializeAnalyzer;
use PromodjSDK\analyzers\track\CommonAnalyzer;
use PromodjSDK\responses\GetTrackDataResponse;

/**
 * Парсинг страницы типа https://promodj.com/dj-egorov/remixes/7184373/The_Black_Eyed_Peas_Imma_Be_Lebedeff_x_Egorov_x_KOFA_Remix
 * Class TrackAnalyzer
 * @package PromodjSDK\analyzers
 */
class TrackAnalyzer extends AggregateAnalyzer
{

    /**
     * @var CommonAnalyzer
     */
    private $commonAnalyzer;

    /**
     * @var DescriptionAnalyzer
     */
    private $descriptionAnalyzer;

    /**
     * @var MusicianUrlAnalyzer
     */
    private $trackMusicianAnalyzer;

    /**
     * @var LinksAnalyzer
     */
    private $linksAnalyzer;

    /**
     * @var PlayerInitializeAnalyzer
     */
    private $playerInitializeDataAnalyzer;

    protected function initDependencies(): void
    {
        $this->commonAnalyzer = new CommonAnalyzer($this->document);
        $this->descriptionAnalyzer = new DescriptionAnalyzer($this->document);
        $this->trackMusicianAnalyzer = new MusicianUrlAnalyzer($this->document);
        $this->linksAnalyzer = new LinksAnalyzer($this->document);
        $this->playerInitializeDataAnalyzer = new PlayerInitializeAnalyzer($this->document);
    }

    /**
     * @return GetTrackDataResponse
     * @throws InvalidSelectorException
     */
    public function service(): Response
    {
        $response = new GetTrackDataResponse();
        $response->common = $this->commonAnalyzer->service();
        $response->description = $this->descriptionAnalyzer->service();
        $response->musician = $this->trackMusicianAnalyzer->service();
        $response->links = $this->linksAnalyzer->service();
        $response->playerInitializeData = $this->playerInitializeDataAnalyzer->service();

        return $response;
    }
}