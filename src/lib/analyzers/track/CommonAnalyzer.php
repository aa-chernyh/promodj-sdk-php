<?php

namespace PromodjSDK\analyzers\track;

use DiDom\Exceptions\InvalidSelectorException;
use PromodjSDK\analyzers\base\Analyzer;
use PromodjSDK\helpers\Dictionary;
use PromodjSDK\helpers\Utils;
use PromodjSDK\models\base\AbstractModel;
use PromodjSDK\models\track\Common;

class CommonAnalyzer extends Analyzer
{

    private const SELECTOR_NAME = '.dj_bblock .dj_universal .generic_title .file_title';
    private const SELECTOR_PROMORANK = '#fvs';
    private const SELECTOR_IS_IN_TOP_100 = '.dj_bblock .dj_universal .generic_title h5 > a > img';
    private const SELECTOR_DOWNLOAD_URL_AROUND_NAME = '#download_flasher';
    private const SELECTOR_DOWNLOAD_M3U_URL = '#content_left div.dj_bblock table.generic_title h5 > span.prelistenm3u > a';
    private const SELECTOR_ON_PDF_FM = '#content_left div.dj_bblock > div.clearfix > div.dj_universal > div';

    /**
     * "В эфире PromoDJ FM с 27 июня 2021 "
     * @var string|null
     */
    private $onPdjFmText;

    protected function analyze(): void
    {
        $this->analyzeOnPdjFmElement();
    }

    /**
     * @throws InvalidSelectorException
     */
    private function analyzeOnPdjFmElement(): void
    {
        $divs = $this->document->find(self::SELECTOR_ON_PDF_FM);
        if(is_array($divs) && count($divs)) {
            foreach ($divs as $div) {
                if(strpos($div->text(), 'В эфире') !== false) {
                    $this->onPdjFmText = str_replace([PHP_EOL, '  '], '', $div->text());
                    return;
                }
            }
        }
    }

    /**
     * @return AbstractModel
     * @throws InvalidSelectorException
     */
    protected function map(): AbstractModel
    {
        $response = new Common();
        $response->name = $this->getName();
        $response->promorank = $this->getPromorank();
        $response->isInTop100 = $this->getIsInTop100();
        $response->downloadUrlAroundName = $this->getDownloadUrlAroundName();
        $response->downloadM3uUrl = $this->getDownloadM3uUrl();
        $response->isOnPdjFM = $this->getIsOnPdjFm();
        $response->onPromodjFmFromDate = $this->getOnPromodjFmFromDate();

        return $response;
    }

    /**
     * @return string|null
     * @throws InvalidSelectorException
     */
    private function getName(): ?string
    {
        $element = $this->document->first(self::SELECTOR_NAME);
        if(!is_null($element)) {
            return $element->text();
        }

        return null;
    }

    /**
     * @return float|null
     * @throws InvalidSelectorException
     */
    private function getPromorank(): ?float
    {
        $element = $this->document->first(self::SELECTOR_PROMORANK);
        if(!is_null($element)) {
            return Utils::getFloatFromString($element->text());
        }

        return null;
    }

    /**
     * @return bool|null
     * @throws InvalidSelectorException
     */
    private function getIsInTop100(): ?bool
    {
        $element = $this->document->first(self::SELECTOR_IS_IN_TOP_100);
        return !is_null($element);
    }

    /**
     * @return string|null
     * @throws InvalidSelectorException
     */
    private function getDownloadUrlAroundName(): ?string
    {
        $element = $this->document->first(self::SELECTOR_DOWNLOAD_URL_AROUND_NAME);
        if(!is_null($element)) {
            return $element->attr('href');
        }

        return null;
    }

    /**
     * @return string|null
     * @throws InvalidSelectorException
     */
    private function getDownloadM3uUrl(): ?string
    {
        $element = $this->document->first(self::SELECTOR_DOWNLOAD_M3U_URL);
        if(!is_null($element)) {
            return $element->attr('href');
        }

        return null;
    }

    private function getIsOnPdjFm(): bool
    {
        return boolval($this->onPdjFmText);
    }

    private function getOnPromodjFmFromDate(): ?string
    {
        if($this->onPdjFmText) {
            return Utils::getDateFromString($this->onPdjFmText)->format(Dictionary::DEFAULT_DATE_FORMAT);
        }

        return null;
    }
}