<?php

namespace PromodjSDK\analyzers\track;

use DiDom\Element;
use DiDom\Exceptions\InvalidSelectorException;
use DOMElement;
use PromodjSDK\analyzers\base\ListAnalyzer;
use PromodjSDK\models\base\AbstractModel;
use PromodjSDK\models\track\DownloadAnchor;

class DescriptionDownloadAnchorAnalyzer extends ListAnalyzer
{

    private const SELECTOR_DJ_UNIVERSAL_DIVS = '#content_left div.dj_bblock > div.clearfix > div.dj_universal > div';

    /**
     * @var Element[]|DOMElement[]|null
     */
    private $downloadAnchorElements;

    /**
     * @throws InvalidSelectorException
     */
    protected function analyze(): void
    {
        $this->analyzeDescriptionBlocks();
    }

    /**
     * @throws InvalidSelectorException
     */
    private function analyzeDescriptionBlocks(): void
    {
        $divs = $this->document->find(self::SELECTOR_DJ_UNIVERSAL_DIVS);
        if(is_array($divs) && count($divs)) {
            foreach ($divs as $div) {
                if(strpos($div->text(), 'Скачать:') !== false) {
                    $this->downloadAnchorElements = $div->find('a');
                }
            }
        }
    }

    /**
     * @return AbstractModel[]
     */
    protected function mapList(): array
    {
        $response = [];
        if(is_array($this->downloadAnchorElements) && count($this->downloadAnchorElements)) {
            foreach ($this->downloadAnchorElements as $downloadAnchorElement) {
                $response[] = $this->mapOne($downloadAnchorElement);
            }
        }

        return $response;
    }

    /**
     * @param Element|DOMElement|null $element
     * @return AbstractModel
     */
    protected function mapOne($element): AbstractModel
    {
        $response = new DownloadAnchor();
        $response->href = $this->getHref($element);
        $response->format = $this->getFormat($element);
        $response->bitrate = $this->getBitrate($element);

        return $response;
    }

    private function getHref($element): ?string
    {
        if($element) {
            return $element->attr('href');
        }

        return null;
    }

    private function getFormat($element): ?string
    {
        if($element) {
            $text = $element->text();
            $formatAndBitrate = explode(',', $text);

            if(count($formatAndBitrate) == 2) {
                return strtolower(trim($formatAndBitrate[0]));
            }
        }

        return null;
    }

    private function getBitrate($element): ?string
    {
        if($element) {
            $text = $element->text();
            $formatAndBitrate = explode(',', $text);

            if(count($formatAndBitrate) == 2) {
                return preg_replace('/[^\d]/', '', $formatAndBitrate[1]);
            }
        }

        return null;
    }
}