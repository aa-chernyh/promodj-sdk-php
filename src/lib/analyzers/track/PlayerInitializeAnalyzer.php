<?php

namespace PromodjSDK\analyzers\track;

use ClientInterface\Base\PhpDocReader\AnnotationException;
use ClientInterface\Base\StructureHelper;
use DiDom\Exceptions\InvalidSelectorException;
use PromodjSDK\analyzers\base\Analyzer;
use PromodjSDK\helpers\Dictionary;
use PromodjSDK\models\base\AbstractModel;
use PromodjSDK\models\track\PlayerInitialize;
use ReflectionException;

/**
 * Не работает для видео
 * Class PlayerInitializeAnalyzer
 * @package PromodjSDK\analyzers\track
 */
class PlayerInitializeAnalyzer extends Analyzer
{

    const SELECTOR_FLASH_PRELISTEN = '#flash_prelisten';

    /**
     * @var string|null
     */
    private $playerInitializeJson;

    protected function analyze(): void
    {
        $this->analyzePlayerInitializeJson();
    }

    /**
     * @throws InvalidSelectorException
     */
    private function analyzePlayerInitializeJson(): void
    {
        $flashPrelistenElement = $this->document->first(self::SELECTOR_FLASH_PRELISTEN);
        if($flashPrelistenElement) {
            $element = $flashPrelistenElement->nextSibling('script');
            if($element) {
                $jsScriptText = $element->text();
                preg_match('/({").+("})/', $jsScriptText, $matches);
                $this->playerInitializeJson = $matches[0] ?? null;
            }
        }
    }

    /**
     * @return AbstractModel
     * @throws AnnotationException
     * @throws ReflectionException
     */
    protected function map(): AbstractModel
    {
        return $this->getPlayerInitializeData();
    }

    /**
     * @return PlayerInitialize|null
     * @throws AnnotationException
     * @throws ReflectionException
     */
    private function getPlayerInitializeData(): PlayerInitialize
    {
        if($this->playerInitializeJson) {
            $array = json_decode($this->playerInitializeJson, true);
            $result = new PlayerInitialize();
            StructureHelper::fill($result, $array, false);
            return $this->formatInitializeData($result);
        }

        return new PlayerInitialize();
    }

    private function formatInitializeData(PlayerInitialize $playerInitialize): PlayerInitialize
    {
        if($playerInitialize->title) {
            $playerInitialize->title = html_entity_decode($playerInitialize->title);
        }

        if(is_array($playerInitialize->sources)) {
            for($i = 0; $i < count($playerInitialize->sources); $i++) {
                $playerInitialize->sources[$i]->waveURL = Dictionary::BASE_URL . $playerInitialize->sources[$i]->waveURL;
                foreach ($playerInitialize->sources[$i]->getAttributes() as $name => $value) {
                    if(stripos($value, 'NULL') !== false) {
                        $playerInitialize->sources[$i]->{$name} = null;
                    }
                }
            }
        }

        return $playerInitialize;
    }
}