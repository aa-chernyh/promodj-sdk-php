<?php

namespace PromodjSDK\analyzers\track;

use DiDom\Element;
use DiDom\Exceptions\InvalidSelectorException;
use DOMElement;
use PromodjSDK\analyzers\base\Analyzer;
use PromodjSDK\models\base\AbstractModel;
use PromodjSDK\models\track\Links;

class LinksAnalyzer extends Analyzer
{

    private const SELECTOR_LINKS_TABLE_TRS = '#content_right > aside div.embeddo_tiny > table.embeddo > tr';

    /**
     * @var Element[]|DOMElement[]|null
     */
    private $linksTableTrs;

    /**
     * @var Links
     */
    private $response;

    protected function initDependencies(): void
    {
        $this->response = new Links();
    }

    protected function analyze(): void
    {
        $this->analyzeLinksTable();
    }

    /**
     * @throws InvalidSelectorException
     */
    private function analyzeLinksTable(): void
    {
        $this->linksTableTrs = $this->document->find(self::SELECTOR_LINKS_TABLE_TRS);
    }

    /**
     * @return AbstractModel
     * @throws InvalidSelectorException
     */
    protected function map(): AbstractModel
    {
        $this->mapLinks();

        return $this->response;
    }

    /**
     * @throws InvalidSelectorException
     */
    private function mapLinks(): void
    {
        if(is_array($this->linksTableTrs) && count($this->linksTableTrs)) {
            foreach ($this->linksTableTrs as $tr) {
                $th = $tr->first('th');
                if($th) {
                    if($this->mapShortLink($th, $tr)) continue;
                    if($this->mapPlayerIframe($th, $tr)) continue;
                    if($this->mapMiniPlayerIframe($th, $tr)) continue;
                }
            }
        }
    }

    /**
     * @param $th Element|DOMElement|null
     * @param $tr Element|DOMElement|null
     * @return bool
     * @throws InvalidSelectorException
     */
    private function mapShortLink($th, $tr): bool
    {
        if(stripos($th->text(), 'Ссылка:') !== false) {
            $td = $tr->first('td > input');
            if($td) {
                $this->response->shortLink = $td->attr('value');
            }

            return true;
        }

        return false;
    }

    /**
     * @param $th Element|DOMElement|null
     * @param $tr Element|DOMElement|null
     * @return bool
     * @throws InvalidSelectorException
     */
    private function mapPlayerIframe($th, $tr): bool
    {
        if(stripos($th->text(), 'Плеер:') !== false) {
            $td = $tr->first('td > input');
            if($td) {
                $this->response->playerIframe = $td->attr('value');
            }

            return true;
        }

        return false;
    }

    /**
     * @param $th Element|DOMElement|null
     * @param $tr Element|DOMElement|null
     * @return bool
     * @throws InvalidSelectorException
     */
    private function mapMiniPlayerIframe($th, $tr): bool
    {
        if(stripos($th->text(), 'Мини:') !== false) {
            $td = $tr->first('td > input');
            if($td) {
                $this->response->miniPlayerIframe = $td->attr('value');
            }

            return true;
        }

        return false;
    }
}