<?php

namespace PromodjSDK\analyzers\track;

use DiDom\Exceptions\InvalidSelectorException;
use PromodjSDK\analyzers\base\Analyzer;
use PromodjSDK\helpers\Dictionary;
use PromodjSDK\models\base\AbstractModel;
use PromodjSDK\models\track\Description;
use PromodjSDK\helpers\Utils;

class DescriptionAnalyzer extends Analyzer
{

    private const SELECTOR_DJ_UNIVERSAL = '#content_left div.dj_bblock > div.clearfix > div.dj_universal';
    private const SELECTOR_DJ_UNIVERSAL_STYLES = self::SELECTOR_DJ_UNIVERSAL . ' > span.styles';

    /**
     * <b>Прослушиваний:</b> 6 698 
     * <b>Скачиваний:</b> 2 222
     * <b>Закладок:</b> 8
     * <b>Стили:</b> 
     * <b>Продолжительность:</b> 2:15
     * <b>Мастеринг, подписан</b>
     * <b>Podsafe-трек</b>
     * <b>Размер:</b> 5.3 Мб
     * <b>Публикация:</b> 25 июня 2021 15:22
     *
     * @var string[]|null
     */
    private $descriptionStrings;
    
    /**
     * @var Description|null
     */
    private $response;

    /**
     * @var DescriptionDownloadAnchorAnalyzer
     */
    private $descriptionDownloadAnchorAnalyzer;

    protected function initDependencies(): void
    {
        $this->descriptionDownloadAnchorAnalyzer = new DescriptionDownloadAnchorAnalyzer($this->document);
    }

    /**
     * @throws InvalidSelectorException
     */
    protected function analyze(): void
    {
        $this->analyzeDescriptionText();
    }

    /**
     * @throws InvalidSelectorException
     */
    private function analyzeDescriptionText(): void
    {
        $descriptionElement = $this->document->first(self::SELECTOR_DJ_UNIVERSAL);
        if($descriptionElement) {
            $innerHtml = str_replace(PHP_EOL, '', $descriptionElement->innerHtml());
            $descriptionText = preg_replace('/(<div.*<\/div>)|(<span.*<\/span>)|(<script.*<\/script>)|(<style.*<\/style>)/mu', '', $innerHtml);
            if($descriptionText) {
                $this->descriptionStrings = explode('<br>', $descriptionText);
            }
        }
    }

    /**
     * @return AbstractModel
     * @throws InvalidSelectorException
     */
    protected function map(): AbstractModel
    {
        $this->response = new Description();
        $this->response->styles = $this->getStyles();
        $this->response->downloadAnchors = $this->descriptionDownloadAnchorAnalyzer->service();

        $this->mapNotWrappedFields();

        return $this->response;
    }

    /**
     * @return string[]
     * @throws InvalidSelectorException
     */
    private function getStyles(): array
    {
        $stylesElement = $this->document->first(self::SELECTOR_DJ_UNIVERSAL_STYLES);
        if($stylesElement) {
            return Utils::getStylesFromString($stylesElement->text());
        }

        return [];
    }

    private function mapNotWrappedFields(): void
    {
        if(is_array($this->descriptionStrings) && count($this->descriptionStrings)) {
            foreach ($this->descriptionStrings as $string) {

                $explodedString = explode('</b>', $string);
                if(is_array($explodedString) && count($explodedString) == 2) {
                    $key = $explodedString[0]; // Например, <b>Прослушиваний:
                    $value = trim($explodedString[1]); // Например, 1 321

                    if($this->mapCountOfListens($key, $value)) continue;
                    if($this->mapCountOfDownloads($key, $value)) continue;
                    if($this->mapCountOfBookmarks($key, $value)) continue;
                    if($this->mapLength($key, $value)) continue;
                    if($this->mapIsMasteredAndIsSigned($key)) continue;
                    if($this->mapIsPodsafe($key)) continue;
                    if($this->mapSizeInMegaBytes($key, $value)) continue;
                    if($this->mapSource($key, $value)) continue;
                    if($this->mapRecordOnDate($key, $value)) continue;
                    if($this->mapPublicationOnDate($key, $value)) continue;
                    if($this->mapIsInCompetition($key)) continue;
                    if($this->mapKey($key, $value)) continue;
                    if($this->mapBpm($key, $value)) continue;
                    if($this->mapRemixOn($key, $value)) continue;
                }
            }
        }
    }

    private function mapCountOfListens(string $key, string $value): bool
    {
        if(
            stripos($key, 'Прослушиваний:') !== false
            || stripos($key, 'Просмотров:') !== false
        ) {
            $this->response->countOfListens = Utils::getNumbersFromString($value);
            return true;
        }

        return false;
    }

    private function mapCountOfDownloads(string $key, string $value): bool
    {
        if(stripos($key, 'Скачиваний:') !== false) {
            $this->response->countOfDownloads = Utils::getNumbersFromString($value);
            return true;
        }

        return false;
    }

    private function mapCountOfBookmarks(string $key, string $value): bool
    {
        if (stripos($key, 'Закладок:') !== false) {
            $this->response->countOfBookmarks = Utils::getNumbersFromString($value);
            return true;
        }

        return false;
    }

    private function mapLength(string $key, string $value): bool
    {
        if(stripos($key, 'Продолжительность:') !== false) {
            $timeInFormat = preg_replace('/[^\d:]/', '', $value);
            $this->response->length = Utils::getSecondsFromTime($timeInFormat);
            return true;
        }

        return false;
    }

    private function mapIsMasteredAndIsSigned(string $key): bool
    {
        if(stripos($key, 'Мастеринг') !== false) {
            $this->response->isMastered = true;

            if(stripos($key, 'подписан') !== false) {
                $this->response->isSigned = true;
            }

            return true;
        }

        return false;
    }

    private function mapIsPodsafe(string $key): bool
    {
        if(stripos($key, 'Podsafe') !== false) {
            $this->response->isPodsafe = true;
            return true;
        }

        return false;
    }

    private function mapSizeInMegaBytes(string $key, string $value): bool
    {
        if(stripos($key, 'Размер:') !== false) {
            $this->response->sizeInMegaBytes = Utils::getFloatFromString($value);
            return true;
        }

        return false;
    }

    private function mapSource(string $key, string $value): bool
    {
        if(stripos($key, 'Источник:') !== false) {
            $this->response->source = $value;
            return true;
        }

        return false;
    }

    private function mapRecordOnDate(string $key, string $value): bool
    {
        if(stripos($key, 'Запись:') !== false) {
            $this->response->recordOnDate = Utils::getDateTimeFromString($value)->format(Dictionary::DEFAULT_DATE_FORMAT);
            return true;
        }

        return false;
    }

    private function mapPublicationOnDate(string $key, string $value): bool
    {
        if(stripos($key, 'Публикация:') !== false) {
            $this->response->publicationOnDate = Utils::getDateTimeFromString($value)->format(Dictionary::DEFAULT_DATE_FORMAT);
            return true;
        }

        return false;
    }

    private function mapIsInCompetition(string $key): bool
    {
        if(stripos($key, 'Конкурс:') !== false) {
            $this->response->isInCompetition = true;
            return true;
        }

        return false;
    }

    private function mapKey(string $key, string $value): bool
    {
        if(stripos($key, 'Тональность:') !== false) {
            $this->response->key = $value;
            return true;
        }

        return false;
    }

    private function mapBpm(string $key, string $value): bool
    {
        if(stripos($key, 'BPM:') !== false) {
            if(preg_match('/\d{2,} – \d{2,}/', $value)) {
                $explodedBpm = explode(' – ', $value);
                if(is_array($explodedBpm) && count($explodedBpm) == 2) {
                    $this->response->bpmFrom = Utils::getNumbersFromString($explodedBpm[0]);
                    $this->response->bpmTo = Utils::getNumbersFromString($explodedBpm[1]);
                }
            } else {
                $this->response->bpm = Utils::getNumbersFromString($value);
            }

            return false;
        }

        return false;
    }

    private function mapRemixOn(string $key, string $value): bool
    {
        if(stripos($key, 'Ремикс на:') !== false) {
            $this->response->remixOn = $value;
            return true;
        }

        return false;
    }
}
