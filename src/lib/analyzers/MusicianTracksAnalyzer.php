<?php

namespace PromodjSDK\analyzers;

use DiDom\Exceptions\InvalidSelectorException;
use PromodjSDK\analyzers\base\AggregateAnalyzer;
use PromodjSDK\analyzers\common\PlayerMiniAnalyzer;
use PromodjSDK\analyzers\musicianTracks\CommonAnalyzer;
use PromodjSDK\analyzers\musicianTracks\GroupsAnalyzer;
use PromodjSDK\responses\GetMusicianTracksResponse;

class MusicianTracksAnalyzer extends AggregateAnalyzer
{

    /**
     * @var CommonAnalyzer
     */
    private $commonAnalyzer;

    /**
     * @var GroupsAnalyzer
     */
    private $groupsAnalyzer;

    /**
     * @var PlayerMiniAnalyzer
     */
    private $playerMiniAnalyzer;

    protected function initDependencies(): void
    {
        $this->commonAnalyzer = new CommonAnalyzer($this->document);
        $this->groupsAnalyzer = new GroupsAnalyzer($this->document);
        $this->playerMiniAnalyzer = new PlayerMiniAnalyzer($this->document);
    }

    /**
     * @return GetMusicianTracksResponse
     * @throws InvalidSelectorException
     */
    public function service(): GetMusicianTracksResponse
    {
        $response = new GetMusicianTracksResponse();
        $response->common = $this->commonAnalyzer->service();
        $response->tracks = $this->playerMiniAnalyzer->service();
        $response->groups = $this->groupsAnalyzer->service();

        return $response;
    }
}