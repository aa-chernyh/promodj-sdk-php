<?php

namespace PromodjSDK\analyzers;

use DiDom\Exceptions\InvalidSelectorException;
use PromodjSDK\analyzers\base\AggregateAnalyzer;
use PromodjSDK\analyzers\common\MusicianUrlAnalyzer;
use PromodjSDK\analyzers\common\PlayerStandardAnalyzer;
use PromodjSDK\responses\GetGroupTracksResponse;

class GroupAnalyzer extends AggregateAnalyzer
{

    private const SELECTOR_TRACK_IN_GROUP = '#content_left div.dj_content div.dj_universal > div.tracks_dump > div.player_standard';

    /**
     * @var MusicianUrlAnalyzer
     */
    private $musicianUrlAnalyzer;

    /**
     * @var PlayerStandardAnalyzer
     */
    private $playerStandardAnalyzer;

    protected function initDependencies(): void
    {
        $this->musicianUrlAnalyzer = new MusicianUrlAnalyzer($this->document);
        $this->playerStandardAnalyzer = new PlayerStandardAnalyzer($this->document, self::SELECTOR_TRACK_IN_GROUP);
    }

    /**
     * @return GetGroupTracksResponse
     * @throws InvalidSelectorException
     */
    public function service(): GetGroupTracksResponse
    {
        $response = new GetGroupTracksResponse();
        $response->musician = $this->musicianUrlAnalyzer->service();
        $response->tracks = $this->playerStandardAnalyzer->service();

        return $response;
    }
}