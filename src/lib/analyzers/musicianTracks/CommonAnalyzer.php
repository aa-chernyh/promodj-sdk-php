<?php

namespace PromodjSDK\analyzers\musicianTracks;

use DiDom\Element;
use DiDom\Exceptions\InvalidSelectorException;
use DOMElement;
use PromodjSDK\analyzers\base\Analyzer;
use PromodjSDK\models\base\AbstractModel;
use PromodjSDK\models\musicianTracks\Common;

class CommonAnalyzer extends Analyzer
{

    private const SELECTOR_GROUP = '#content_left div.dj_content div.dj_universal > div.dj_universal';

    /**
     * @var Element|DOMElement|null
     */
    private $groupElementWithHiddenElements;

    protected function analyze(): void
    {
        $this->analyzeGroupElements();
    }

    /**
     * @throws InvalidSelectorException
     */
    private function analyzeGroupElements(): void
    {
        $groupElements = $this->document->find(self::SELECTOR_GROUP);
        if(is_array($groupElements) && count($groupElements)) {
            foreach ($groupElements as $groupElement) {
                if($groupElement->has('a.hot')) {
                    $this->groupElementWithHiddenElements = $groupElement;
                    return;
                }
            }
        }
    }

    protected function map(): AbstractModel
    {
        $response = new Common();
        $response->countTracksInShownGroup = $this->getCountTracksInShownGroup();

        return $response;
    }

    private function getCountTracksInShownGroup(): ?int
    {
        if($this->groupElementWithHiddenElements) {
            $trackElements = $this->groupElementWithHiddenElements->find('div.tracks_dump > div.player_mini');
            if($trackElements && ($count = count($trackElements))) {
                return $count;
            }
        }

        return null;
    }
}