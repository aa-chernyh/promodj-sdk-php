<?php

namespace PromodjSDK\analyzers\musicianTracks;

use DiDom\Element;
use DiDom\Exceptions\InvalidSelectorException;
use DOMElement;
use PromodjSDK\analyzers\base\ListAnalyzer;
use PromodjSDK\models\base\AbstractModel;
use PromodjSDK\models\musicianTracks\Group;

class GroupsAnalyzer extends ListAnalyzer
{

    private const SELECTOR_GROUP = '#content_left div.dj_content div.dj_universal > div.dj_universal';

    /**
     * @var Element[]|DOMElement[]|null
     */
    private $allMaterialsWrappers;

    protected function analyze(): void
    {
        $this->analyzeGroupElements();
    }

    /**
     * @throws InvalidSelectorException
     */
    private function analyzeGroupElements(): void
    {
        $groupElements = $this->document->find(self::SELECTOR_GROUP);
        if(is_array($groupElements) && count($groupElements)) {
            foreach ($groupElements as $groupElement) {
                if($groupElement->has('a.hot')) {
                    $this->allMaterialsWrappers[] = $groupElement;
                }
            }
        }
    }

    /**
     * @return AbstractModel[]
     * @throws InvalidSelectorException
     */
    protected function mapList(): array
    {
        $response = [];
        if(is_array($this->allMaterialsWrappers) && count($this->allMaterialsWrappers)) {
            foreach ($this->allMaterialsWrappers as $allMaterialsWrapper) {
                $response[] = $this->mapOne($allMaterialsWrapper);
            }
        }

        return $this->getUniqueGroups($response);
    }

    /**
     * @param Group[] $groups
     * @return array
     */
    private function getUniqueGroups(array $groups): array
    {
        $result = [];
        $issetHrefs = [];
        foreach ($groups as $group) {
            if(!in_array($group->href, $issetHrefs)) {
                $result[] = $group;
                $issetHrefs[] = $group->href;
            }
        }

        return $result;
    }

    /**
     * @param Element|DOMElement|null $element
     * @return AbstractModel
     * @throws InvalidSelectorException
     */
    protected function mapOne($element): AbstractModel
    {
        $result = new Group();
        $result->href = $this->getHref($element);
        $result->count = $this->getCount($element);

        return $result;
    }

    /**
     * @param $element Element|DOMElement|null
     * @return string|null
     * @throws InvalidSelectorException
     */
    private function getHref($element): ?string
    {
        if($element) {
            $hrefElement = $element->first('a.hot');
            if($hrefElement) {
                return $hrefElement->attr('href');
            }
        }

        return null;
    }

    /**
     * @param $element Element|DOMElement|null
     * @return int|null
     * @throws InvalidSelectorException
     */
    private function getCount($element): ?int
    {
        if($element) {
            $countElement = $element->first('span.small');
            if($countElement) {
                return intval($countElement->text());
            }
        }

        return null;
    }
}