<?php

namespace PromodjSDK\analyzers;

use DiDom\Exceptions\InvalidSelectorException;
use PromodjSDK\analyzers\base\AggregateAnalyzer;
use PromodjSDK\analyzers\common\MusicianUrlAnalyzer;
use PromodjSDK\analyzers\musician\CommonAnalyzer;
use PromodjSDK\analyzers\musician\StylesAnalyzer;
use PromodjSDK\responses\GetMusicianDataResponse;

/**
 * Парсинг страницы типа https://promodj.com/dimixer/contact
 * Class MusicianAnalyzer
 * @package PromodjSDK\analyzers
 */
class MusicianAnalyzer extends AggregateAnalyzer
{

    /**
     * @var MusicianUrlAnalyzer
     */
    private $musicianUrlAnalyzer;

    /**
     * @var CommonAnalyzer
     */
    private $commonAnalyzer;

    /**
     * @var StylesAnalyzer
     */
    private $stylesAnalyzer;

    protected function initDependencies(): void
    {
        $this->musicianUrlAnalyzer = new MusicianUrlAnalyzer($this->document);
        $this->commonAnalyzer = new CommonAnalyzer($this->document);
        $this->stylesAnalyzer = new StylesAnalyzer($this->document);
    }

    /**
     * @return GetMusicianDataResponse
     * @throws InvalidSelectorException
     */
    public function service(): GetMusicianDataResponse
    {
        $response = new GetMusicianDataResponse();
        $response->anchor = $this->musicianUrlAnalyzer->service();
        $response->common = $this->commonAnalyzer->service();
        $response->styles = $this->stylesAnalyzer->service();

        return $response;
    }
}