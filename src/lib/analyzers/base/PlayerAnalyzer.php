<?php

namespace PromodjSDK\analyzers\base;

use DiDom\Document;
use DiDom\Element;
use DiDom\Exceptions\InvalidSelectorException;
use DOMElement;
use PromodjSDK\models\base\AbstractModel;
use PromodjSDK\models\common\TrackShort;

abstract class PlayerAnalyzer extends ListAnalyzer
{

    /**
     * @var Element[]|DOMElement[]|null
     */
    private $trackWrapperElements;

    /**
     * @var string
     */
    private $playersSelector;

    public function __construct(Document $document, ?string $playersSelector = null)
    {
        parent::__construct($document);
        $this->playersSelector = $playersSelector ?: $this->getTrackWrappersSelector();
    }

    protected function analyze(): void
    {
        $this->analyzeTracks();
    }

    /**
     * @throws InvalidSelectorException
     */
    private function analyzeTracks(): void
    {
        $this->trackWrapperElements = $this->document->find($this->playersSelector);
    }

    abstract protected function getTrackWrappersSelector(): string;

    /**
     * @return array
     * @throws InvalidSelectorException
     */
    protected function mapList(): array
    {
        $response = [];
        if(is_array($this->trackWrapperElements) && count($this->trackWrapperElements)) {
            foreach ($this->trackWrapperElements as $trackWrapperElement) {
                $response[] = $this->mapOne($trackWrapperElement);
            }
        }

        return $response;
    }

    /**
     * @param Element|DOMElement|null $element
     * @return TrackShort
     * @throws InvalidSelectorException
     */
    protected function mapOne($element): AbstractModel
    {
        $result = new TrackShort();
        $result->href = $this->getHref($element);
        $result->name = $this->getName($element);
        $result->countOfListens = $this->getCountOfListens($element);
        $result->length = $this->getLength($element);
        $result->countOfComments = $this->getCountOfComments($element);
        $result->downloadMp3OrMovUrl = $this->getDownloadMp3OrMovUrl($element);
        $result->countOfDownloads = $this->getCountOfDownloads($element);
        $result->promorank = $this->getPromorank($element);
        $result->styles = $this->getStyles($element);
        $result->isVideo = $this->getIsVideo($element);

        return $result;
    }

    /**
     * @param Element|DOMElement|null $element
     * @return string|null
     * @throws InvalidSelectorException
     */
    abstract protected function getHref($element): ?string;

    /**
     * @param Element|DOMElement|null $element
     * @return string|null
     * @throws InvalidSelectorException
     */
    abstract protected function getName($element): ?string;

    /**
     * @param Element|DOMElement|null $element
     * @return int|null
     * @throws InvalidSelectorException
     */
    abstract protected function getCountOfListens($element): ?int;

    /**
     * @param Element|DOMElement|null $element
     * @return int|null
     * @throws InvalidSelectorException
     */
    abstract protected function getLength($element): ?int;

    /**
     * @param Element|DOMElement|null $element
     * @return int|null
     * @throws InvalidSelectorException
     */
    abstract protected function getCountOfComments($element): ?int;

    /**
     * @param Element|DOMElement|null $element
     * @return string|null
     * @throws InvalidSelectorException
     */
    abstract protected function getDownloadMp3OrMovUrl($element): ?string;

    /**
     * @param Element|DOMElement|null $element
     * @return int|null
     * @throws InvalidSelectorException
     */
    abstract protected function getCountOfDownloads($element): ?int;

    /**
     * @param Element|DOMElement|null $element
     * @return float|null
     * @throws InvalidSelectorException
     */
    abstract protected function getPromorank($element): ?float;

    /**
     * @param Element|DOMElement|null $element
     * @return string[]|null
     * @throws InvalidSelectorException
     */
    abstract protected function getStyles($element): ?array;

    /**
     * @param Element|DOMElement|null $element
     * @return bool
     */
    abstract protected function getIsVideo($element): bool;
}