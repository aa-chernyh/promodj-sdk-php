<?php

namespace PromodjSDK\analyzers\base;

interface IAnalyzer
{
    public function service();
}