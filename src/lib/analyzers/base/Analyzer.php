<?php

namespace PromodjSDK\analyzers\base;

use DiDom\Document;
use DiDom\Exceptions\InvalidSelectorException;
use PromodjSDK\models\base\AbstractModel;

abstract class Analyzer implements IAnalyzer
{

    /**
     * @var Document
     */
    protected $document;

    public function __construct(Document $document)
    {
        $this->document = $document;
        $this->initDependencies();
    }

    protected function initDependencies(): void
    {

    }

    /**
     * @throws InvalidSelectorException
     */
    final public function service(): AbstractModel
    {
        $this->analyze();
        return $this->map();
    }

    /**
     * @throws InvalidSelectorException
     */
    abstract protected function analyze(): void;

    abstract protected function map(): AbstractModel;
}