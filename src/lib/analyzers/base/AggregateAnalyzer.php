<?php

namespace PromodjSDK\analyzers\base;

use DiDom\Document;

abstract class AggregateAnalyzer implements IAnalyzer
{
    /**
     * @var Document
     */
    public $document;

    public function __construct(string $html)
    {
        $this->document = new Document($html);
        $this->initDependencies();
    }

    abstract protected function initDependencies(): void;
}