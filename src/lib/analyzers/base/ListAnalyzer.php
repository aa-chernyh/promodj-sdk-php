<?php

namespace PromodjSDK\analyzers\base;

use DiDom\Document;
use DiDom\Element;
use DiDom\Exceptions\InvalidSelectorException;
use DOMElement;
use PromodjSDK\models\base\AbstractModel;

abstract class ListAnalyzer implements IAnalyzer
{

    /**
     * @var Document
     */
    protected $document;

    public function __construct(Document $document)
    {
        $this->document = $document;
        $this->initDependencies();
    }

    protected function initDependencies(): void
    {

    }

    /**
     * @return AbstractModel[]
     * @throws InvalidSelectorException
     */
    final public function service(): array
    {
        $this->analyze();
        return $this->mapList();
    }

    /**
     * @throws InvalidSelectorException
     */
    abstract protected function analyze(): void;

    /**
     * @return AbstractModel[]
     */
    abstract protected function mapList(): array;

    /**
     * @param $element Element|DOMElement|null
     * @return AbstractModel
     */
    abstract protected function mapOne($element): AbstractModel;
}