<?php

namespace PromodjSDK\common;

use ReflectionClass;
use ReflectionProperty;

class AbstractStruct
{
    public function toArray(): array
    {
        return json_decode(json_encode($this), true);
    }

    public function getAttributes(): array
    {
        $reflection = new ReflectionClass($this);

        $result = [];
        foreach ($reflection->getProperties(ReflectionProperty::IS_PUBLIC) as $property) {
            $result[$property->getName()] = $property->getValue($this);
        }
        return $result;
    }
}