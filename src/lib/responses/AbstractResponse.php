<?php

namespace PromodjSDK\responses;

use ClientInterface\Response;
use PromodjSDK\common\AbstractStruct;
use PromodjSDK\models\base\AbstractModel;
use ReflectionClass;
use ReflectionProperty;

abstract class AbstractResponse extends AbstractStruct implements Response
{

    /**
     * @var array
     */
    private $errors = [];

    public function getErrors(): array
    {
        return $this->errors;
    }

    protected function setErrors(string $modelName, array $errors, ?int $iterator = null): void
    {
        if(is_integer($iterator)) {
            $this->errors[$modelName][$iterator] = $errors;
        } else {
            $this->errors[$modelName] = $errors;
        }
    }

    public function isSuccess(): bool
    {
        foreach ($this->getAttributes() as $name => $value) {
            /** @var AbstractModel $value */
            if(is_array($value)) {
                for ($i = 0; $i < count($value); $i++) {
                    $this->validateValue($name, $value[$i], $i);
                }
            } else {
                $this->validateValue($name, $value);
            }
        }

        return empty($this->getErrors());
    }

    private function validateValue(string $propertyName, AbstractModel $value, ?int $iterator = null): void
    {
        if(!$value->validate()) {
            $this->setErrors($propertyName, $value->getErrors(), $iterator);
        }
    }
}