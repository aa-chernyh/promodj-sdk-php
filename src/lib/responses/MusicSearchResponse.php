<?php

namespace PromodjSDK\responses;

use PromodjSDK\models\common\TrackShort;
use PromodjSDK\models\musicSearch\Common;

class MusicSearchResponse extends AbstractResponse
{

    /**
     * @var Common
     */
    public $common;

    /**
     * @var TrackShort[]
     */
    public $tracks;
}