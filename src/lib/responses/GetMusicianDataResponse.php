<?php

namespace PromodjSDK\responses;

use PromodjSDK\models\common\MusicianUrl;
use PromodjSDK\models\musician\Common;
use PromodjSDK\models\musician\Styles;

class GetMusicianDataResponse extends AbstractResponse
{

    /**
     * @var MusicianUrl
     */
    public $anchor;

    /**
     * @var Common
     */
    public $common;

    /**
     * @var Styles
     */
    public $styles;
}