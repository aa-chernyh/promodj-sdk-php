<?php

namespace PromodjSDK\responses;

use PromodjSDK\models\track\Links;
use PromodjSDK\models\common\MusicianUrl;
use PromodjSDK\models\track\PlayerInitialize;
use PromodjSDK\models\track\Common;
use PromodjSDK\models\track\Description;

class GetTrackDataResponse extends AbstractResponse
{

    /**
     * @var Common
     */
    public $common;

    /**
     * @var MusicianUrl
     */
    public $musician;

    /**
     * @var Description
     */
    public $description;

    /**
     * @var PlayerInitialize|null
     */
    public $playerInitializeData;

    /**
     * @var Links|null
     */
    public $links;
}