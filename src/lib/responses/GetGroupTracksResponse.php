<?php

namespace PromodjSDK\responses;

use PromodjSDK\models\common\MusicianUrl;
use PromodjSDK\models\common\TrackShort;

class GetGroupTracksResponse extends AbstractResponse
{

    /**
     * @var MusicianUrl
     */
    public $musician;

    /**
     * @var TrackShort[]
     */
    public $tracks;
}