<?php

namespace PromodjSDK\responses;

use PromodjSDK\models\musicianTracks\Common;
use PromodjSDK\models\musicianTracks\Group;
use PromodjSDK\models\common\TrackShort;

class GetMusicianTracksResponse extends AbstractResponse
{

    /**
     * @var Common
     */
    public $common;

    /**
     * @var Group[]
     */
    public $groups;

    /**
     * @var TrackShort[]
     */
    public $tracks;
}