<?php

namespace PromodjSDK\enums;

use PromodjSDK\exceptions\LogicException;

class Kinds
{
    public const TRACK = 'track';
    public const REMIX = 'remix';
    public const MIX = 'mix';
    public const PODCAST = 'podcast';
    public const RADIOSHOW = 'radioshow';
    public const LIVE = 'live';
    public const ACAPELLA = 'acapella';
    public const SAMPLE = 'sample';
    public const VIDEO = 'video';
    public const PROMO = 'promo';
    public const REALTONE = 'realtone';

    public const TRACK_NAME = 'Трек';
    public const REMIX_NAME = 'Ремикс';
    public const MIX_NAME = 'Микс';
    public const PODCAST_NAME = 'Подкаст';
    public const RADIOSHOW_NAME = 'Радиошоу';
    public const LIVE_NAME = 'Лайв';
    public const ACAPELLA_NAME = 'Акапелла';
    public const SAMPLE_NAME = 'Сэмпл';
    public const VIDEO_NAME = 'Видео';
    public const PROMO_NAME = 'Промо';
    public const REALTONE_NAME = 'Реалтон';

    public const LIST = [
        self::TRACK,
        self::REMIX,
        self::MIX,
        self::PODCAST,
        self::RADIOSHOW,
        self::LIVE,
        self::ACAPELLA,
        self::SAMPLE,
        self::VIDEO,
        self::PROMO,
        self::REALTONE,
    ];

    public const NAME_LIST = [
        self::TRACK_NAME,
        self::REMIX_NAME,
        self::MIX_NAME,
        self::PODCAST_NAME,
        self::RADIOSHOW_NAME,
        self::LIVE_NAME,
        self::ACAPELLA_NAME,
        self::SAMPLE_NAME,
        self::VIDEO_NAME,
        self::PROMO_NAME,
        self::REALTONE_NAME,
    ];

    public const MAPPING = [
        self::TRACK => self::TRACK_NAME,
        self::REMIX => self::REMIX_NAME,
        self::MIX => self::MIX_NAME,
        self::PODCAST => self::PODCAST_NAME,
        self::RADIOSHOW => self::RADIOSHOW_NAME,
        self::LIVE => self::LIVE_NAME,
        self::ACAPELLA => self::ACAPELLA_NAME,
        self::SAMPLE => self::SAMPLE_NAME,
        self::VIDEO => self::VIDEO_NAME,
        self::PROMO => self::PROMO_NAME,
        self::REALTONE => self::REALTONE_NAME,
    ];

    /**
     * @return array
     * @throws LogicException
     */
    public static function getSections(): array
    {
        $result = [];
        foreach (self::LIST as $kind) {
            $result[] = self::getMultipleKind($kind);
        }

        return $result;
    }

    /**
     * @param string $kind
     * @return string
     * @throws LogicException
     */
    public static function getMultipleKind(string $kind): string
    {
        if(!in_array($kind, self::LIST)) {
            throw new LogicException("$kind - не музыкальный тип");
        }

        $ending = substr($kind, -1) == 'x' ? 'es' : 's';
        return $kind . $ending;
    }
}