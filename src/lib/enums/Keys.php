<?php

namespace PromodjSDK\enums;

class Keys
{

    public const A = 'A';
    public const A_FLAT = 'Ab';
    public const A_SHARP = 'A#';
    public const B = 'B';
    public const B_FLAT = 'Bb';
    public const C = 'C';
    public const C_SHARP = 'C#';
    public const D = 'D';
    public const E_FLAT = 'Eb';
    public const E = 'E';
    public const F = 'F';
    public const F_SHARP = 'F#';
    public const G = 'G';
    public const Am = 'Am';
    public const Am_FLAT = 'Abm';
    public const Bm = 'Bm';
    public const Bm_FLAT = 'Bbm';
    public const Cm = 'Cm';
    public const Cm_SHARP = 'C#m';
    public const Dm = 'Dm';
    public const Em_FLAT = 'Ebm';
    public const Em = 'Em';
    public const Fm = 'Fm';
    public const Fm_SHARP = 'F#m';
    public const Gm = 'Gm';
    public const Gm_SHARP = 'G#m';

    public const LIST = [
        self::A,
        self::A_FLAT,
        self::A_SHARP,
        self::B,
        self::B_FLAT,
        self::C,
        self::C_SHARP,
        self::D,
        self::E_FLAT,
        self::E,
        self::F,
        self::F_SHARP,
        self::G,
        self::Am,
        self::Am_FLAT,
        self::Bm,
        self::Bm_FLAT,
        self::Cm,
        self::Cm_SHARP,
        self::Dm,
        self::Em_FLAT,
        self::Em,
        self::Fm,
        self::Fm_SHARP,
        self::Gm,
        self::Gm_SHARP,
    ];
}