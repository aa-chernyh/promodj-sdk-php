<?php

namespace PromodjSDK\enums;

class VideoTypes
{

    public const CLIP = 'videoclip';
    public const SHOW = 'videoshow';

    public const CLIP_NAME = 'видеоклип';
    public const SHOW_NAME = 'видеошоу';

    public const LIST = [
        self::CLIP,
        self::SHOW,
    ];

    public const NAME_LIST = [
        self::CLIP_NAME,
        self::SHOW_NAME,
    ];

    public const MAPPING = [
        self::CLIP => self::CLIP_NAME,
        self::SHOW => self::SHOW_NAME,
    ];
}