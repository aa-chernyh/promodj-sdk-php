<?php

namespace PromodjSDK\enums;

class Styles
{
    public const _2_STEP = "2_step";
    public const _8BIT = "8bit";
    public const ABSTRACT_HIP_HOP = "abstract_hip-hop";
    public const ACID = "acid";
    public const ACID_BREAKS = "acid_breaks";
    public const ACID_HOUSE = "acid_house";
    public const ACID_JAZZ = "acid_jazz";
    public const ACID_TECHNO = "acid_techno";
    public const ACID_TRANCE = "acid_trance";
    public const ACOUSTIC = "acoustic";
    public const AFRO_HOUSE = "afro_house";
    public const AGGROTECH = "aggrotech";
    public const ALTERNATIVE_RAP = "alternative_rap";
    public const ALTERNATIVE_ROCK = "alternative_rock";
    public const AMBIENT = "ambient";
    public const AMBIENT_BREAKS = "ambient_breaks";
    public const AMBIENT_DUB = "ambient_dub";
    public const AMBIENT_HOUSE = "ambient_house";
    public const ART_ROCK = "art_rock";
    public const ATMOSPHERIC_BREAKS = "atmospheric_breaks";
    public const BAILEFUNK = "bailefunk";
    public const BALEARIC_BEAT = "balearic_beat";
    public const BASS_HOUSE = "bass_house";
    public const BASSLINE = "bassline";
    public const BEATBOXING = "beatboxing";
    public const BERLIN_SCHOOL = "berlin_school";
    public const BIG_BEAT = "big_beat";
    public const BIG_ROOM_HOUSE = "big_room_house";
    public const BLUES = "blues";
    public const BOUNCY_TECHNO = "bouncy_techno";
    public const BREAKCORE = "breakcore";
    public const BREAK_BEAT = "break_beat";
    public const BREAKSTEP = "breakstep";
    public const BRITISH_RAP = "british_rap";
    public const BRITPOP = "britpop";
    public const BROKENBEAT = "brokenbeat";
    public const BROSTEP = "brostep";
    public const CHICAGO_HOUSE = "chicago_house";
    public const CHILLOUT = "chillout";
    public const CHILLSTEP = "chillstep";
    public const CHILLWAVE = "chillwave";
    public const CHIPTUNE = "chiptune";
    public const CLASSICAL_CROSSOVER = "classical_crossover";
    public const CLUB_HOUSE = "club_house";
    public const COMEDY_RAP = "comedy_rap";
    public const COMPLEXTRO = "complextro";
    public const CRUNK = "crunk";
    public const DANCE_POP = "dance_pop";
    public const DANCECORE = "dancecore";
    public const DANCEHALL = "dancehall";
    public const DARK_AMBIENT = "dark_ambient";
    public const DARK_PROGRESSIVE = "dark_progressive";
    public const DARK_PSY_TRANCE = "dark_psy_trance";
    public const DARKSTEP = "darkstep";
    public const DEEP_HOUSE = "deep_house";
    public const DEEP_TECHNO = "deep_techno";
    public const DETROIT_TECHNO = "detroit_techno";
    public const DIGITAL_HARDCORE = "digital_hardcore";
    public const DIRTY_RAP = "dirty_rap";
    public const DISCO = "disco";
    public const DISCO_HOUSE = "disco_house";
    public const DISS = "diss";
    public const DOWNTEMPO = "downtempo";
    public const DRILL_N_BASS = "drill_n_bass";
    public const DNB = "dnb";
    public const DRUMFUNK = "drumfunk";
    public const DUB = "dub";
    public const DUB_TECHNO = "dub_techno";
    public const DUBSTEP = "dubstep";
    public const DUBWISE = "dubwise";
    public const DUTCH_HOUSE = "dutch_house";
    public const EAST_COAST_RAP = "east_coast_rap";
    public const EASY_LISTENING = "easy_listening";
    public const ELECTRO2 = "electro2";
    public const ELECTROHOUSE = "electrohouse";
    public const ELECTRO_PROGRESSIVE = "electro_progressive";
    public const ELECTRO_SWING = "electro_swing";
    public const ELECTRO_TECHNO = "electro_techno";
    public const ELECTRO_PUNK = "electro_punk";
    public const ELECTROCLASH = "electroclash";
    public const EBM = "ebm";
    public const EURO_TECHNO = "euro_techno";
    public const EURO_TRANCE = "euro_trance";
    public const EURODANCE = "eurodance";
    public const EXPERIMENTAL = "experimental";
    public const FIDGET = "fidget";
    public const FLORIDA_BREAKS = "florida_breaks";
    public const FOOTWORK = "footwork";
    public const FREESTYLE = "freestyle";
    public const FRENCH_ELECTRO = "french_electro";
    public const FRENCH_HOUSE = "french_house";
    public const FRENCHCORE = "frenchcore";
    public const FULL_ON = "full_on";
    public const FUNK = "funk";
    public const FUNKY_BREAKS = "funky_breaks";
    public const FUNKY_HOUSE = "funky_house";
    public const FUNKY_TECHNO = "funky_techno";
    public const FUTURE_BASS = "future_bass";
    public const FUTURE_GARAGE = "future_garage";
    public const FUTURE_HOUSE = "future_house";
    public const FUTUREPOP = "futurepop";
    public const G_FUNK = "g_funk";
    public const G_HOUSE = "g_house";
    public const GANGSTA_RAP = "gangsta_rap";
    public const GARAGE = "garage";
    public const GHETTO_HOUSE = "ghetto_house";
    public const GHETTOTECH = "ghettotech";
    public const GLAM_ROCK = "glam_rock";
    public const GLITCH = "glitch";
    public const GLITCH_HOP = "glitch_hop";
    public const GOA = "goa";
    public const GOTHIC_ROCK = "gothic_rock";
    public const GRIME = "grime";
    public const GRUNGE = "grunge";
    public const HAPPY_HARDCORE = "happy_hardcore";
    public const HARDHOUSE = "hardhouse";
    public const HARD_IDM = "hard_idm";
    public const HARD_ROCK = "hard_rock";
    public const HARD_TECHNO = "hard_techno";
    public const HARDTRANCE = "hardtrance";
    public const HARDCORE = "hardcore";
    public const HARDCORE_CONTINUUM = "hardcore_continuum";
    public const HARDCORE_RAP = "hardcore_rap";
    public const HARDSTEP = "hardstep";
    public const HARDSTYLE = "hardstyle";
    public const HEAVY_METAL = "heavy_metal";
    public const HI_NRG = "hi_nrg";
    public const HIP_HOUSE = "hip_house";
    public const HIP_HOP = "hip_hop";
    public const HORRORCORE = "horrorcore";
    public const HOUSE = "house";
    public const IDM = "idm";
    public const ILLBIENT = "illbient";
    public const INDIE_DANCE = "indie_dance";
    public const INDIE_ROCK = "indie_rock";
    public const INDIETRONICA = "indietronica";
    public const INDUSTRIAL = "industrial";
    public const INDUSTRIAL_TECHNO = "industrial_techno";
    public const INSTRUMENTAL = "instrumental";
    public const INTELLIGENT = "intelligent";
    public const ITALO_DISCO = "italo_disco";
    public const JAZZ = "jazz";
    public const JAZZ_RAP = "jazz-rap";
    public const JAZZSTEP = "jazzstep";
    public const JUMP_UP = "jump-up";
    public const JUMPSTYLE = "jumpstyle";
    public const JUNGLE = "jungle";
    public const KRAUTROCK = "krautrock";
    public const LATIN_HOUSE = "latin_house";
    public const LATIN_POP = "latin_pop";
    public const LEFT_FIELD_HOUSE = "left_field_house";
    public const LEFTFIELD_BASS = "leftfield_bass";
    public const LIQUID_FUNK = "liquid_funk";
    public const LIVE_LOOPING = "live_looping";
    public const LOFI = "lofi";
    public const LOUNGE = "lounge";
    public const LOWERCASE = "lowercase";
    public const MELBOURNE_BOUNCE = "melbourne_bounce";
    public const MELODIC_TRANCE = "melodic_trance";
    public const MIAMI_BASS = "miami_bass";
    public const MICROHOUSE = "microhouse";
    public const MINIMAL_TECHNO = "minimal_techno";
    public const MINIMAL_PSYTRANCE = "minimal_psytrance";
    public const MOOMBAHCORE = "moombahcore";
    public const MOOMBAHSOUL = "moombahsoul";
    public const MOOMBAHTON = "moombahton";
    public const MUSIC_CONCRETE = "music_concrete";
    public const NEO_SOUL = "neo-soul";
    public const NEOTRANCE = "neotrance";
    public const NEUROFUNK = "neurofunk";
    public const NEW_AGE = "new_age";
    public const NEW_BEAT = "new_beat";
    public const NEW_RAVE = "new_rave";
    public const NOISE = "noise";
    public const NORTEC = "nortec";
    public const NU_BREAKS = "nu_breaks";
    public const NU_DISCO = "nu_disco";
    public const NU_JAZZ = "nu_jazz";
    public const NU_METAL = "nu_metal";
    public const OLD_SCHOOL_RAP = "old_school_rap";
    public const POP = "pop";
    public const POP_RAP = "pop_rap";
    public const POP_ROCK = "pop_rock";
    public const POST_DUBSTEP = "post_dubstep";
    public const POST_PUNK = "post_punk";
    public const POST_ROCK = "post_rock";
    public const POWER_NOISE = "power_noise";
    public const PROGRESSIVE_BREAKS = "progressive_breaks";
    public const PROGRESSIVE_HOUSE = "progressive_house";
    public const PROGRESSIVE_TRANCE = "progressive_trance";
    public const PROGRESSIVE_ROCK = "progressive_rock";
    public const PSY_CHILL = "psy_chill";
    public const PSYCHEDELIC = "psychedelic";
    public const PSYBIENT = "psybient";
    public const PSY_BREAKS = "psy_breaks";
    public const PSYCHEDELIC_ROCK = "psychedelic_rock";
    public const PUMPING = "pumping";
    public const PUNK_ROCK = "punk_rock";
    public const RNB = "rnb";
    public const RAGGAJUNGLE = "raggajungle";
    public const RAGGACORE = "raggacore";
    public const RAVE = "rave";
    public const REGGAE = "reggae";
    public const REGGAETON = "reggaeton";
    public const ROCK = "rock";
    public const ROCK_N_ROLL = "rock_n_roll";
    public const ROCKABILLY = "rockabilly";
    public const RUSSIAN_POP = "russian_pop";
    public const SAMBASS = "sambass";
    public const SCHRANZ = "schranz";
    public const SCOUSE_HOUSE = "scouse_house";
    public const SHOEGAZING = "shoegazing";
    public const SKA = "ska";
    public const SLOW_MOTION_HOUSE_DISCO = "slow_motion_house_disco";
    public const SMOOTH_JAZZ = "smooth_jazz";
    public const SOUL = "soul";
    public const SOULFUL_HOUSE = "soulful_house";
    public const SOUTHERN_RAP = "southern_rap";
    public const SPACE_DISCO = "space_disco";
    public const SPACESYNTH = "spacesynth";
    public const SPEED_GARAGE = "speed_garage";
    public const SPEEDCORE = "speedcore";
    public const STONER_ROCK = "stoner_rock";
    public const SURF = "surf";
    public const SYNTH_POP = "synth_pop";
    public const SYNTHWAVE = "synthwave";
    public const TECHHOUSE = "techhouse";
    public const TECH_TRANCE = "tech_trance";
    public const TECHNO = "techno";
    public const TECHNOID = "technoid";
    public const TECHSTEP = "techstep";
    public const TEEN_POP = "teen_pop";
    public const TRANCE = "trance";
    public const TRANCESTEP = "trancestep";
    public const TRAP = "trap";
    public const TRIBAL_HOUSE = "tribal_house";
    public const TRIP_HOP = "trip_hop";
    public const TROPICAL_HOUSE = "tropical_house";
    public const TURNTABLISM = "turntablism";
    public const TWERK = "twerk";
    public const UK_FUNKY = "uk_funky";
    public const UK_GARAGE = "uk_garage";
    public const UPLIFTING_TRANCE = "uplifting_trance";
    public const VOCAL_HOUSE = "vocal_house";
    public const VOCAL_TRANCE = "vocal_trance";
    public const WEST_COAST_RAP = "west_coast_rap";
    public const WITCH_HOUSE = "witch_house";
    public const WONKY = "wonky";
    public const YORKSHIRE_BLEEPS_AND_BASS = "yorkshire_bleeps_and_bass";
    public const ANALYTICS = "analytics";
    public const INTERVIEW = "interview";
    public const NEWS = "news";
    public const REVIEW = "review";
    public const TALK = "talk";
    public const ELECTROPOP = "electropop";
    public const DISCO_FUNK = 'disco_funk';
    public const BOOTY_BREAKS = 'booty_breaks';
    public const INDIE = 'indie';
    public const FUTURE_JAZZ = 'future_jazz';
    public const MINIMAL_HOUSE = 'minimal_house';
    public const BUMPING = 'bumping';

    public const _2_STEP_NAME = "2 Step";
    public const _8BIT_NAME = "8-bit";
    public const ABSTRACT_HIP_HOP_NAME = "Abstract Hip-Hop";
    public const ACID_NAME = "Acid";
    public const ACID_BREAKS_NAME = "Acid Breaks";
    public const ACID_HOUSE_NAME = "Acid House";
    public const ACID_JAZZ_NAME = "Acid Jazz";
    public const ACID_TECHNO_NAME = "Acid Techno";
    public const ACID_TRANCE_NAME = "Acid Trance";
    public const ACOUSTIC_NAME = "Acoustic";
    public const AFRO_HOUSE_NAME = "Afro House";
    public const AGGROTECH_NAME = "Aggrotech";
    public const ALTERNATIVE_RAP_NAME = "Alternative Rap";
    public const ALTERNATIVE_ROCK_NAME = "Alternative Rock";
    public const AMBIENT_NAME = "Ambient";
    public const AMBIENT_BREAKS_NAME = "Ambient Breaks";
    public const AMBIENT_DUB_NAME = "Ambient Dub";
    public const AMBIENT_HOUSE_NAME = "Ambient House";
    public const ART_ROCK_NAME = "Art Rock";
    public const ATMOSPHERIC_BREAKS_NAME = "Atmospheric Breaks";
    public const BAILEFUNK_NAME = "Baile Funk";
    public const BALEARIC_BEAT_NAME = "Balearic Beat";
    public const BASS_HOUSE_NAME = "Bass House";
    public const BASSLINE_NAME = "Bassline";
    public const BEATBOXING_NAME = "Beatboxing";
    public const BERLIN_SCHOOL_NAME = "Berlin School";
    public const BIG_BEAT_NAME = "Big Beat";
    public const BIG_ROOM_HOUSE_NAME = "Big Room House";
    public const BLUES_NAME = "Blues";
    public const BOUNCY_TECHNO_NAME = "Bouncy Techno";
    public const BREAKCORE_NAME = "Breakcore";
    public const BREAK_BEAT_NAME = "Breaks";
    public const BREAKSTEP_NAME = "Breakstep";
    public const BRITISH_RAP_NAME = "British Rap";
    public const BRITPOP_NAME = "Britpop";
    public const BROKENBEAT_NAME = "Brokenbeat";
    public const BROSTEP_NAME = "Brostep";
    public const CHICAGO_HOUSE_NAME = "Chicago House";
    public const CHILLOUT_NAME = "Chillout";
    public const CHILLSTEP_NAME = "Chillstep";
    public const CHILLWAVE_NAME = "Chillwave";
    public const CHIPTUNE_NAME = "Chiptune";
    public const CLASSICAL_CROSSOVER_NAME = "Classical Crossover";
    public const CLUB_HOUSE_NAME = "Club House";
    public const COMEDY_RAP_NAME = "Comedy Rap";
    public const COMPLEXTRO_NAME = "Complextro";
    public const CRUNK_NAME = "Crunk";
    public const DANCE_POP_NAME = "Dance Pop";
    public const DANCECORE_NAME = "Dancecore";
    public const DANCEHALL_NAME = "Dancehall";
    public const DARK_AMBIENT_NAME = "Dark Ambient";
    public const DARK_PROGRESSIVE_NAME = "Dark Progressive";
    public const DARK_PSY_TRANCE_NAME = "Dark Psy Trance";
    public const DARKSTEP_NAME = "Darkstep";
    public const DEEP_HOUSE_NAME = "Deep House";
    public const DEEP_TECHNO_NAME = "Deep Techno";
    public const DETROIT_TECHNO_NAME = "Detroit Techno";
    public const DIGITAL_HARDCORE_NAME = "Digital Hardcore";
    public const DIRTY_RAP_NAME = "Dirty Rap";
    public const DISCO_NAME = "Disco";
    public const DISCO_HOUSE_NAME = "Disco House";
    public const DISS_NAME = "Diss";
    public const DOWNTEMPO_NAME = "Downtempo";
    public const DRILL_N_BASS_NAME = "Drill'n'bass";
    public const DNB_NAME = "Drum & Bass";
    public const DRUMFUNK_NAME = "Drumfunk";
    public const DUB_NAME = "Dub";
    public const DUB_TECHNO_NAME = "Dub Techno";
    public const DUBSTEP_NAME = "Dubstep";
    public const DUBWISE_NAME = "Dubwise";
    public const DUTCH_HOUSE_NAME = "Dutch House";
    public const EAST_COAST_RAP_NAME = "East Coast Rap";
    public const EASY_LISTENING_NAME = "Easy Listening";
    public const ELECTRO2_NAME = "Electro";
    public const ELECTROHOUSE_NAME = "Electro House";
    public const ELECTRO_PROGRESSIVE_NAME = "Electro Progressive";
    public const ELECTRO_SWING_NAME = "Electro Swing";
    public const ELECTRO_TECHNO_NAME = "Electro Techno";
    public const ELECTRO_PUNK_NAME = "Electro-punk";
    public const ELECTROCLASH_NAME = "Electroclash";
    public const EBM_NAME = "Electronic Body Music";
    public const EURO_TECHNO_NAME = "Euro Techno";
    public const EURO_TRANCE_NAME = "Euro Trance";
    public const EURODANCE_NAME = "Eurodance";
    public const EXPERIMENTAL_NAME = "Experimental";
    public const FIDGET_NAME = "Fidget House";
    public const FLORIDA_BREAKS_NAME = "Florida Breaks";
    public const FOOTWORK_NAME = "Footwork";
    public const FREESTYLE_NAME = "Freestyle";
    public const FRENCH_ELECTRO_NAME = "French Electro";
    public const FRENCH_HOUSE_NAME = "French House";
    public const FRENCHCORE_NAME = "Frenchcore";
    public const FULL_ON_NAME = "Full-On";
    public const FUNK_NAME = "Funk";
    public const FUNKY_BREAKS_NAME = "Funky Breaks";
    public const FUNKY_HOUSE_NAME = "Funky House";
    public const FUNKY_TECHNO_NAME = "Funky Techno";
    public const FUTURE_BASS_NAME = "Future Bass";
    public const FUTURE_GARAGE_NAME = "Future Garage";
    public const FUTURE_HOUSE_NAME = "Future House";
    public const FUTUREPOP_NAME = "Futurepop";
    public const G_FUNK_NAME = "G-Funk";
    public const G_HOUSE_NAME = "G-House";
    public const GANGSTA_RAP_NAME = "Gangsta Rap";
    public const GARAGE_NAME = "Garage";
    public const GHETTO_HOUSE_NAME = "Ghetto House";
    public const GHETTOTECH_NAME = "Ghettotech";
    public const GLAM_ROCK_NAME = "Glam Rock";
    public const GLITCH_NAME = "Glitch";
    public const GLITCH_HOP_NAME = "Glitch Hop";
    public const GOA_NAME = "Goa Trance";
    public const GOTHIC_ROCK_NAME = "Gothic Rock";
    public const GRIME_NAME = "Grime";
    public const GRUNGE_NAME = "Grunge";
    public const HAPPY_HARDCORE_NAME = "Happy Hardcore";
    public const HARDHOUSE_NAME = "Hard House";
    public const HARD_IDM_NAME = "Hard IDM";
    public const HARD_ROCK_NAME = "Hard Rock";
    public const HARD_TECHNO_NAME = "Hard Techno";
    public const HARDTRANCE_NAME = "Hard Trance";
    public const HARDCORE_NAME = "Hardcore";
    public const HARDCORE_CONTINUUM_NAME = "Hardcore Continuum ";
    public const HARDCORE_RAP_NAME = "Hardcore rap";
    public const HARDSTEP_NAME = "Hardstep";
    public const HARDSTYLE_NAME = "Hardstyle";
    public const HEAVY_METAL_NAME = "Heavy metal";
    public const HI_NRG_NAME = "Hi-NRG";
    public const HIP_HOUSE_NAME = "Hip House";
    public const HIP_HOP_NAME = "Hip-hop/Rap";
    public const HORRORCORE_NAME = "Horrorcore";
    public const HOUSE_NAME = "House";
    public const IDM_NAME = "IDM";
    public const ILLBIENT_NAME = "Illbient";
    public const INDIE_DANCE_NAME = "Indie Dance";
    public const INDIE_ROCK_NAME = "Indie rock";
    public const INDIETRONICA_NAME = "Indietronica";
    public const INDUSTRIAL_NAME = "Industrial";
    public const INDUSTRIAL_TECHNO_NAME = "Industrial Techno";
    public const INSTRUMENTAL_NAME = "Instrumental";
    public const INTELLIGENT_NAME = "Intelligent";
    public const ITALO_DISCO_NAME = "Italo Disco";
    public const JAZZ_NAME = "Jazz";
    public const JAZZ_RAP_NAME = "Jazz-Rap";
    public const JAZZSTEP_NAME = "Jazzstep";
    public const JUMP_UP_NAME = "Jump Up";
    public const JUMPSTYLE_NAME = "JumpStyle";
    public const JUNGLE_NAME = "Jungle";
    public const KRAUTROCK_NAME = "Krautrock";
    public const LATIN_HOUSE_NAME = "Latin House";
    public const LATIN_POP_NAME = "Latin Pop";
    public const LEFT_FIELD_HOUSE_NAME = "Left-field House";
    public const LEFTFIELD_BASS_NAME = "Leftfield Bass";
    public const LIQUID_FUNK_NAME = "Liquid funk";
    public const LIVE_LOOPING_NAME = "Live Looping";
    public const LOFI_NAME = "Lo-Fi";
    public const LOUNGE_NAME = "Lounge";
    public const LOWERCASE_NAME = "Lowercase";
    public const MELBOURNE_BOUNCE_NAME = "Melbourne Bounce";
    public const MELODIC_TRANCE_NAME = "Melodic Trance";
    public const MIAMI_BASS_NAME = "Miami Bass";
    public const MICROHOUSE_NAME = "Microhouse";
    public const MINIMAL_TECHNO_NAME = "Minimal Techno";
    public const MINIMAL_PSYTRANCE_NAME = "Minimal psytrance";
    public const MOOMBAHCORE_NAME = "Moombahcore";
    public const MOOMBAHSOUL_NAME = "Moombahsoul";
    public const MOOMBAHTON_NAME = "Moombahton";
    public const MUSIC_CONCRETE_NAME = "Music Concrete";
    public const NEO_SOUL_NAME = "Neo-Soul";
    public const NEOTRANCE_NAME = "Neotrance";
    public const NEUROFUNK_NAME = "Neurofunk";
    public const NEW_AGE_NAME = "New Age";
    public const NEW_BEAT_NAME = "New Beat";
    public const NEW_RAVE_NAME = "New Rave";
    public const NOISE_NAME = "Noise";
    public const NORTEC_NAME = "Nortec";
    public const NU_BREAKS_NAME = "Nu Breaks";
    public const NU_DISCO_NAME = "Nu Disco";
    public const NU_JAZZ_NAME = "Nu Jazz";
    public const NU_METAL_NAME = "Nu metal";
    public const OLD_SCHOOL_RAP_NAME = "Old School Rap";
    public const POP_NAME = "Pop";
    public const POP_RAP_NAME = "Pop Rap";
    public const POP_ROCK_NAME = "Pop Rock";
    public const POST_DUBSTEP_NAME = "Post Dubstep";
    public const POST_PUNK_NAME = "Post-punk";
    public const POST_ROCK_NAME = "Post-rock";
    public const POWER_NOISE_NAME = "Power Noise";
    public const PROGRESSIVE_BREAKS_NAME = "Progressive Breaks";
    public const PROGRESSIVE_HOUSE_NAME = "Progressive House";
    public const PROGRESSIVE_TRANCE_NAME = "Progressive Trance";
    public const PROGRESSIVE_ROCK_NAME = "Progressive rock";
    public const PSY_CHILL_NAME = "Psy Chill";
    public const PSYCHEDELIC_NAME = "Psy Trance";
    public const PSYBIENT_NAME = "Psybient";
    public const PSY_BREAKS_NAME = "Psychedelic breakbeat";
    public const PSYCHEDELIC_ROCK_NAME = "Psychedelic rock";
    public const PUMPING_NAME = "Pumping House";
    public const PUNK_ROCK_NAME = "Punk rock";
    public const RNB_NAME = "R&B";
    public const RAGGAJUNGLE_NAME = "Ragga Jungle";
    public const RAGGACORE_NAME = "Raggacore";
    public const RAVE_NAME = "Rave";
    public const REGGAE_NAME = "Reggae";
    public const REGGAETON_NAME = "Reggaeton";
    public const ROCK_NAME = "Rock";
    public const ROCK_N_ROLL_NAME = "Rock'n'roll";
    public const ROCKABILLY_NAME = "Rockabilly";
    public const RUSSIAN_POP_NAME = "Russian Pop";
    public const SAMBASS_NAME = "Sambass";
    public const SCHRANZ_NAME = "Schranz";
    public const SCOUSE_HOUSE_NAME = "Scouse House";
    public const SHOEGAZING_NAME = "Shoegazing";
    public const SKA_NAME = "Ska";
    public const SLOW_MOTION_HOUSE_DISCO_NAME = "Slow Motion House (Disco)";
    public const SMOOTH_JAZZ_NAME = "Smooth Jazz";
    public const SOUL_NAME = "Soul";
    public const SOULFUL_HOUSE_NAME = "Soulful House";
    public const SOUTHERN_RAP_NAME = "Southern Rap";
    public const SPACE_DISCO_NAME = "Space Disco";
    public const SPACESYNTH_NAME = "Spacesynth";
    public const SPEED_GARAGE_NAME = "Speed Garage";
    public const SPEEDCORE_NAME = "Speedcore";
    public const STONER_ROCK_NAME = "Stoner rock";
    public const SURF_NAME = "Surf";
    public const SYNTH_POP_NAME = "Synth-Pop";
    public const SYNTHWAVE_NAME = "Synthwave";
    public const TECHHOUSE_NAME = "Tech House";
    public const TECH_TRANCE_NAME = "Tech Trance";
    public const TECHNO_NAME = "Techno";
    public const TECHNOID_NAME = "Technoid";
    public const TECHSTEP_NAME = "Techstep";
    public const TEEN_POP_NAME = "Teen Pop";
    public const TRANCE_NAME = "Trance";
    public const TRANCESTEP_NAME = "Trancestep";
    public const TRAP_NAME = "Trap";
    public const TRIBAL_HOUSE_NAME = "Tribal House";
    public const TRIP_HOP_NAME = "Trip-Hop";
    public const TROPICAL_HOUSE_NAME = "Tropical House";
    public const TURNTABLISM_NAME = "Turntablism";
    public const TWERK_NAME = "Twerk";
    public const UK_FUNKY_NAME = "UK Funky";
    public const UK_GARAGE_NAME = "UK Garage";
    public const UPLIFTING_TRANCE_NAME = "Uplifting Trance";
    public const VOCAL_HOUSE_NAME = "Vocal House";
    public const VOCAL_TRANCE_NAME = "Vocal Trance";
    public const WEST_COAST_RAP_NAME = "West Coast Rap";
    public const WITCH_HOUSE_NAME = "Witch House";
    public const WONKY_NAME = "Wonky";
    public const YORKSHIRE_BLEEPS_AND_BASS_NAME = "Yorkshire Bleeps and Bass";
    public const ANALYTICS_NAME = "Аналитика";
    public const INTERVIEW_NAME = "Интервью";
    public const NEWS_NAME = "Новости";
    public const REVIEW_NAME = "Обзор";
    public const TALK_NAME = "Разговорный";
    public const ELECTROPOP_NAME = "Electropop";
    public const DISCO_FUNK_NAME = 'Disco Funk';
    public const BOOTY_BREAKS_NAME = 'Booty breaks';
    public const INDIE_NAME = 'Indie';
    public const FUTURE_JAZZ_NAME = 'Future Jazz';
    public const MINIMAL_HOUSE_NAME = 'Minimal House';
    public const BUMPING_NAME = 'Bumping';

    public const MAPPING = [
        self::_2_STEP => self::_2_STEP_NAME,
        self::_8BIT => self::_8BIT_NAME,
        self::ABSTRACT_HIP_HOP => self::ABSTRACT_HIP_HOP_NAME,
        self::ACID => self::ACID_NAME,
        self::ACID_BREAKS => self::ACID_BREAKS_NAME,
        self::ACID_HOUSE => self::ACID_HOUSE_NAME,
        self::ACID_JAZZ => self::ACID_JAZZ_NAME,
        self::ACID_TECHNO => self::ACID_TECHNO_NAME,
        self::ACID_TRANCE => self::ACID_TRANCE_NAME,
        self::ACOUSTIC => self::ACOUSTIC_NAME,
        self::AFRO_HOUSE => self::AFRO_HOUSE_NAME,
        self::AGGROTECH => self::AGGROTECH_NAME,
        self::ALTERNATIVE_RAP => self::ALTERNATIVE_RAP_NAME,
        self::ALTERNATIVE_ROCK => self::ALTERNATIVE_ROCK_NAME,
        self::AMBIENT => self::AMBIENT_NAME,
        self::AMBIENT_BREAKS => self::AMBIENT_BREAKS_NAME,
        self::AMBIENT_DUB => self::AMBIENT_DUB_NAME,
        self::AMBIENT_HOUSE => self::AMBIENT_HOUSE_NAME,
        self::ART_ROCK => self::ART_ROCK_NAME,
        self::ATMOSPHERIC_BREAKS => self::ATMOSPHERIC_BREAKS_NAME,
        self::BAILEFUNK => self::BAILEFUNK_NAME,
        self::BALEARIC_BEAT => self::BALEARIC_BEAT_NAME,
        self::BASS_HOUSE => self::BASS_HOUSE_NAME,
        self::BASSLINE => self::BASSLINE_NAME,
        self::BEATBOXING => self::BEATBOXING_NAME,
        self::BERLIN_SCHOOL => self::BERLIN_SCHOOL_NAME,
        self::BIG_BEAT => self::BIG_BEAT_NAME,
        self::BIG_ROOM_HOUSE => self::BIG_ROOM_HOUSE_NAME,
        self::BLUES => self::BLUES_NAME,
        self::BOUNCY_TECHNO => self::BOUNCY_TECHNO_NAME,
        self::BREAKCORE => self::BREAKCORE_NAME,
        self::BREAK_BEAT => self::BREAK_BEAT_NAME,
        self::BREAKSTEP => self::BREAKSTEP_NAME,
        self::BRITISH_RAP => self::BRITISH_RAP_NAME,
        self::BRITPOP => self::BRITPOP_NAME,
        self::BROKENBEAT => self::BROKENBEAT_NAME,
        self::BROSTEP => self::BROSTEP_NAME,
        self::CHICAGO_HOUSE => self::CHICAGO_HOUSE_NAME,
        self::CHILLOUT => self::CHILLOUT_NAME,
        self::CHILLSTEP => self::CHILLSTEP_NAME,
        self::CHILLWAVE => self::CHILLWAVE_NAME,
        self::CHIPTUNE => self::CHIPTUNE_NAME,
        self::CLASSICAL_CROSSOVER => self::CLASSICAL_CROSSOVER_NAME,
        self::CLUB_HOUSE => self::CLUB_HOUSE_NAME,
        self::COMEDY_RAP => self::COMEDY_RAP_NAME,
        self::COMPLEXTRO => self::COMPLEXTRO_NAME,
        self::CRUNK => self::CRUNK_NAME,
        self::DANCE_POP => self::DANCE_POP_NAME,
        self::DANCECORE => self::DANCECORE_NAME,
        self::DANCEHALL => self::DANCEHALL_NAME,
        self::DARK_AMBIENT => self::DARK_AMBIENT_NAME,
        self::DARK_PROGRESSIVE => self::DARK_PROGRESSIVE_NAME,
        self::DARK_PSY_TRANCE => self::DARK_PSY_TRANCE_NAME,
        self::DARKSTEP => self::DARKSTEP_NAME,
        self::DEEP_HOUSE => self::DEEP_HOUSE_NAME,
        self::DEEP_TECHNO => self::DEEP_TECHNO_NAME,
        self::DETROIT_TECHNO => self::DETROIT_TECHNO_NAME,
        self::DIGITAL_HARDCORE => self::DIGITAL_HARDCORE_NAME,
        self::DIRTY_RAP => self::DIRTY_RAP_NAME,
        self::DISCO => self::DISCO_NAME,
        self::DISCO_HOUSE => self::DISCO_HOUSE_NAME,
        self::DISS => self::DISS_NAME,
        self::DOWNTEMPO => self::DOWNTEMPO_NAME,
        self::DRILL_N_BASS => self::DRILL_N_BASS_NAME,
        self::DNB => self::DNB_NAME,
        self::DRUMFUNK => self::DRUMFUNK_NAME,
        self::DUB => self::DUB_NAME,
        self::DUB_TECHNO => self::DUB_TECHNO_NAME,
        self::DUBSTEP => self::DUBSTEP_NAME,
        self::DUBWISE => self::DUBWISE_NAME,
        self::DUTCH_HOUSE => self::DUTCH_HOUSE_NAME,
        self::EAST_COAST_RAP => self::EAST_COAST_RAP_NAME,
        self::EASY_LISTENING => self::EASY_LISTENING_NAME,
        self::ELECTRO2 => self::ELECTRO2_NAME,
        self::ELECTROHOUSE => self::ELECTROHOUSE_NAME,
        self::ELECTRO_PROGRESSIVE => self::ELECTRO_PROGRESSIVE_NAME,
        self::ELECTRO_SWING => self::ELECTRO_SWING_NAME,
        self::ELECTRO_TECHNO => self::ELECTRO_TECHNO_NAME,
        self::ELECTRO_PUNK => self::ELECTRO_PUNK_NAME,
        self::ELECTROCLASH => self::ELECTROCLASH_NAME,
        self::EBM => self::EBM_NAME,
        self::EURO_TECHNO => self::EURO_TECHNO_NAME,
        self::EURO_TRANCE => self::EURO_TRANCE_NAME,
        self::EURODANCE => self::EURODANCE_NAME,
        self::EXPERIMENTAL => self::EXPERIMENTAL_NAME,
        self::FIDGET => self::FIDGET_NAME,
        self::FLORIDA_BREAKS => self::FLORIDA_BREAKS_NAME,
        self::FOOTWORK => self::FOOTWORK_NAME,
        self::FREESTYLE => self::FREESTYLE_NAME,
        self::FRENCH_ELECTRO => self::FRENCH_ELECTRO_NAME,
        self::FRENCH_HOUSE => self::FRENCH_HOUSE_NAME,
        self::FRENCHCORE => self::FRENCHCORE_NAME,
        self::FULL_ON => self::FULL_ON_NAME,
        self::FUNK => self::FUNK_NAME,
        self::FUNKY_BREAKS => self::FUNKY_BREAKS_NAME,
        self::FUNKY_HOUSE => self::FUNKY_HOUSE_NAME,
        self::FUNKY_TECHNO => self::FUNKY_TECHNO_NAME,
        self::FUTURE_BASS => self::FUTURE_BASS_NAME,
        self::FUTURE_GARAGE => self::FUTURE_GARAGE_NAME,
        self::FUTURE_HOUSE => self::FUTURE_HOUSE_NAME,
        self::FUTUREPOP => self::FUTUREPOP_NAME,
        self::G_FUNK => self::G_FUNK_NAME,
        self::G_HOUSE => self::G_HOUSE_NAME,
        self::GANGSTA_RAP => self::GANGSTA_RAP_NAME,
        self::GARAGE => self::GARAGE_NAME,
        self::GHETTO_HOUSE => self::GHETTO_HOUSE_NAME,
        self::GHETTOTECH => self::GHETTOTECH_NAME,
        self::GLAM_ROCK => self::GLAM_ROCK_NAME,
        self::GLITCH => self::GLITCH_NAME,
        self::GLITCH_HOP => self::GLITCH_HOP_NAME,
        self::GOA => self::GOA_NAME,
        self::GOTHIC_ROCK => self::GOTHIC_ROCK_NAME,
        self::GRIME => self::GRIME_NAME,
        self::GRUNGE => self::GRUNGE_NAME,
        self::HAPPY_HARDCORE => self::HAPPY_HARDCORE_NAME,
        self::HARDHOUSE => self::HARDHOUSE_NAME,
        self::HARD_IDM => self::HARD_IDM_NAME,
        self::HARD_ROCK => self::HARD_ROCK_NAME,
        self::HARD_TECHNO => self::HARD_TECHNO_NAME,
        self::HARDTRANCE => self::HARDTRANCE_NAME,
        self::HARDCORE => self::HARDCORE_NAME,
        self::HARDCORE_CONTINUUM => self::HARDCORE_CONTINUUM_NAME,
        self::HARDCORE_RAP => self::HARDCORE_RAP_NAME,
        self::HARDSTEP => self::HARDSTEP_NAME,
        self::HARDSTYLE => self::HARDSTYLE_NAME,
        self::HEAVY_METAL => self::HEAVY_METAL_NAME,
        self::HI_NRG => self::HI_NRG_NAME,
        self::HIP_HOUSE => self::HIP_HOUSE_NAME,
        self::HIP_HOP => self::HIP_HOP_NAME,
        self::HORRORCORE => self::HORRORCORE_NAME,
        self::HOUSE => self::HOUSE_NAME,
        self::IDM => self::IDM_NAME,
        self::ILLBIENT => self::ILLBIENT_NAME,
        self::INDIE_DANCE => self::INDIE_DANCE_NAME,
        self::INDIE_ROCK => self::INDIE_ROCK_NAME,
        self::INDIETRONICA => self::INDIETRONICA_NAME,
        self::INDUSTRIAL => self::INDUSTRIAL_NAME,
        self::INDUSTRIAL_TECHNO => self::INDUSTRIAL_TECHNO_NAME,
        self::INSTRUMENTAL => self::INSTRUMENTAL_NAME,
        self::INTELLIGENT => self::INTELLIGENT_NAME,
        self::ITALO_DISCO => self::ITALO_DISCO_NAME,
        self::JAZZ => self::JAZZ_NAME,
        self::JAZZ_RAP => self::JAZZ_RAP_NAME,
        self::JAZZSTEP => self::JAZZSTEP_NAME,
        self::JUMP_UP => self::JUMP_UP_NAME,
        self::JUMPSTYLE => self::JUMPSTYLE_NAME,
        self::JUNGLE => self::JUNGLE_NAME,
        self::KRAUTROCK => self::KRAUTROCK_NAME,
        self::LATIN_HOUSE => self::LATIN_HOUSE_NAME,
        self::LATIN_POP => self::LATIN_POP_NAME,
        self::LEFT_FIELD_HOUSE => self::LEFT_FIELD_HOUSE_NAME,
        self::LEFTFIELD_BASS => self::LEFTFIELD_BASS_NAME,
        self::LIQUID_FUNK => self::LIQUID_FUNK_NAME,
        self::LIVE_LOOPING => self::LIVE_LOOPING_NAME,
        self::LOFI => self::LOFI_NAME,
        self::LOUNGE => self::LOUNGE_NAME,
        self::LOWERCASE => self::LOWERCASE_NAME,
        self::MELBOURNE_BOUNCE => self::MELBOURNE_BOUNCE_NAME,
        self::MELODIC_TRANCE => self::MELODIC_TRANCE_NAME,
        self::MIAMI_BASS => self::MIAMI_BASS_NAME,
        self::MICROHOUSE => self::MICROHOUSE_NAME,
        self::MINIMAL_TECHNO => self::MINIMAL_TECHNO_NAME,
        self::MINIMAL_PSYTRANCE => self::MINIMAL_PSYTRANCE_NAME,
        self::MOOMBAHCORE => self::MOOMBAHCORE_NAME,
        self::MOOMBAHSOUL => self::MOOMBAHSOUL_NAME,
        self::MOOMBAHTON => self::MOOMBAHTON_NAME,
        self::MUSIC_CONCRETE => self::MUSIC_CONCRETE_NAME,
        self::NEO_SOUL => self::NEO_SOUL_NAME,
        self::NEOTRANCE => self::NEOTRANCE_NAME,
        self::NEUROFUNK => self::NEUROFUNK_NAME,
        self::NEW_AGE => self::NEW_AGE_NAME,
        self::NEW_BEAT => self::NEW_BEAT_NAME,
        self::NEW_RAVE => self::NEW_RAVE_NAME,
        self::NOISE => self::NOISE_NAME,
        self::NORTEC => self::NORTEC_NAME,
        self::NU_BREAKS => self::NU_BREAKS_NAME,
        self::NU_DISCO => self::NU_DISCO_NAME,
        self::NU_JAZZ => self::NU_JAZZ_NAME,
        self::NU_METAL => self::NU_METAL_NAME,
        self::OLD_SCHOOL_RAP => self::OLD_SCHOOL_RAP_NAME,
        self::POP => self::POP_NAME,
        self::POP_RAP => self::POP_RAP_NAME,
        self::POP_ROCK => self::POP_ROCK_NAME,
        self::POST_DUBSTEP => self::POST_DUBSTEP_NAME,
        self::POST_PUNK => self::POST_PUNK_NAME,
        self::POST_ROCK => self::POST_ROCK_NAME,
        self::POWER_NOISE => self::POWER_NOISE_NAME,
        self::PROGRESSIVE_BREAKS => self::PROGRESSIVE_BREAKS_NAME,
        self::PROGRESSIVE_HOUSE => self::PROGRESSIVE_HOUSE_NAME,
        self::PROGRESSIVE_TRANCE => self::PROGRESSIVE_TRANCE_NAME,
        self::PROGRESSIVE_ROCK => self::PROGRESSIVE_ROCK_NAME,
        self::PSY_CHILL => self::PSY_CHILL_NAME,
        self::PSYCHEDELIC => self::PSYCHEDELIC_NAME,
        self::PSYBIENT => self::PSYBIENT_NAME,
        self::PSY_BREAKS => self::PSY_BREAKS_NAME,
        self::PSYCHEDELIC_ROCK => self::PSYCHEDELIC_ROCK_NAME,
        self::PUMPING => self::PUMPING_NAME,
        self::PUNK_ROCK => self::PUNK_ROCK_NAME,
        self::RNB => self::RNB_NAME,
        self::RAGGAJUNGLE => self::RAGGAJUNGLE_NAME,
        self::RAGGACORE => self::RAGGACORE_NAME,
        self::RAVE => self::RAVE_NAME,
        self::REGGAE => self::REGGAE_NAME,
        self::REGGAETON => self::REGGAETON_NAME,
        self::ROCK => self::ROCK_NAME,
        self::ROCK_N_ROLL => self::ROCK_N_ROLL_NAME,
        self::ROCKABILLY => self::ROCKABILLY_NAME,
        self::RUSSIAN_POP => self::RUSSIAN_POP_NAME,
        self::SAMBASS => self::SAMBASS_NAME,
        self::SCHRANZ => self::SCHRANZ_NAME,
        self::SCOUSE_HOUSE => self::SCOUSE_HOUSE_NAME,
        self::SHOEGAZING => self::SHOEGAZING_NAME,
        self::SKA => self::SKA_NAME,
        self::SLOW_MOTION_HOUSE_DISCO => self::SLOW_MOTION_HOUSE_DISCO_NAME,
        self::SMOOTH_JAZZ => self::SMOOTH_JAZZ_NAME,
        self::SOUL => self::SOUL_NAME,
        self::SOULFUL_HOUSE => self::SOULFUL_HOUSE_NAME,
        self::SOUTHERN_RAP => self::SOUTHERN_RAP_NAME,
        self::SPACE_DISCO => self::SPACE_DISCO_NAME,
        self::SPACESYNTH => self::SPACESYNTH_NAME,
        self::SPEED_GARAGE => self::SPEED_GARAGE_NAME,
        self::SPEEDCORE => self::SPEEDCORE_NAME,
        self::STONER_ROCK => self::STONER_ROCK_NAME,
        self::SURF => self::SURF_NAME,
        self::SYNTH_POP => self::SYNTH_POP_NAME,
        self::SYNTHWAVE => self::SYNTHWAVE_NAME,
        self::TECHHOUSE => self::TECHHOUSE_NAME,
        self::TECH_TRANCE => self::TECH_TRANCE_NAME,
        self::TECHNO => self::TECHNO_NAME,
        self::TECHNOID => self::TECHNOID_NAME,
        self::TECHSTEP => self::TECHSTEP_NAME,
        self::TEEN_POP => self::TEEN_POP_NAME,
        self::TRANCE => self::TRANCE_NAME,
        self::TRANCESTEP => self::TRANCESTEP_NAME,
        self::TRAP => self::TRAP_NAME,
        self::TRIBAL_HOUSE => self::TRIBAL_HOUSE_NAME,
        self::TRIP_HOP => self::TRIP_HOP_NAME,
        self::TROPICAL_HOUSE => self::TROPICAL_HOUSE_NAME,
        self::TURNTABLISM => self::TURNTABLISM_NAME,
        self::TWERK => self::TWERK_NAME,
        self::UK_FUNKY => self::UK_FUNKY_NAME,
        self::UK_GARAGE => self::UK_GARAGE_NAME,
        self::UPLIFTING_TRANCE => self::UPLIFTING_TRANCE_NAME,
        self::VOCAL_HOUSE => self::VOCAL_HOUSE_NAME,
        self::VOCAL_TRANCE => self::VOCAL_TRANCE_NAME,
        self::WEST_COAST_RAP => self::WEST_COAST_RAP_NAME,
        self::WITCH_HOUSE => self::WITCH_HOUSE_NAME,
        self::WONKY => self::WONKY_NAME,
        self::YORKSHIRE_BLEEPS_AND_BASS => self::YORKSHIRE_BLEEPS_AND_BASS_NAME,
        self::ANALYTICS => self::ANALYTICS_NAME,
        self::INTERVIEW => self::INTERVIEW_NAME,
        self::NEWS => self::NEWS_NAME,
        self::REVIEW => self::REVIEW_NAME,
        self::TALK => self::TALK_NAME,
        self::ELECTROPOP => self::ELECTROPOP_NAME,
        self::DISCO_FUNK => self::DISCO_FUNK_NAME,
        self::BOOTY_BREAKS => self::BOOTY_BREAKS_NAME,
        self::INDIE => self::INDIE_NAME,
        self::FUTURE_JAZZ => self::FUTURE_JAZZ_NAME,
        self::MINIMAL_HOUSE => self::MINIMAL_HOUSE_NAME,
        self::BUMPING => self::BUMPING_NAME,
    ];

    public const LIST = [
        self::_2_STEP,
        self::_8BIT,
        self::ABSTRACT_HIP_HOP,
        self::ACID,
        self::ACID_BREAKS,
        self::ACID_HOUSE,
        self::ACID_JAZZ,
        self::ACID_TECHNO,
        self::ACID_TRANCE,
        self::ACOUSTIC,
        self::AFRO_HOUSE,
        self::AGGROTECH,
        self::ALTERNATIVE_RAP,
        self::ALTERNATIVE_ROCK,
        self::AMBIENT,
        self::AMBIENT_BREAKS,
        self::AMBIENT_DUB,
        self::AMBIENT_HOUSE,
        self::ART_ROCK,
        self::ATMOSPHERIC_BREAKS,
        self::BAILEFUNK,
        self::BALEARIC_BEAT,
        self::BASS_HOUSE,
        self::BASSLINE,
        self::BEATBOXING,
        self::BERLIN_SCHOOL,
        self::BIG_BEAT,
        self::BIG_ROOM_HOUSE,
        self::BLUES,
        self::BOUNCY_TECHNO,
        self::BREAKCORE,
        self::BREAK_BEAT,
        self::BREAKSTEP,
        self::BRITISH_RAP,
        self::BRITPOP,
        self::BROKENBEAT,
        self::BROSTEP,
        self::CHICAGO_HOUSE,
        self::CHILLOUT,
        self::CHILLSTEP,
        self::CHILLWAVE,
        self::CHIPTUNE,
        self::CLASSICAL_CROSSOVER,
        self::CLUB_HOUSE,
        self::COMEDY_RAP,
        self::COMPLEXTRO,
        self::CRUNK,
        self::DANCE_POP,
        self::DANCECORE,
        self::DANCEHALL,
        self::DARK_AMBIENT,
        self::DARK_PROGRESSIVE,
        self::DARK_PSY_TRANCE,
        self::DARKSTEP,
        self::DEEP_HOUSE,
        self::DEEP_TECHNO,
        self::DETROIT_TECHNO,
        self::DIGITAL_HARDCORE,
        self::DIRTY_RAP,
        self::DISCO,
        self::DISCO_HOUSE,
        self::DISS,
        self::DOWNTEMPO,
        self::DRILL_N_BASS,
        self::DNB,
        self::DRUMFUNK,
        self::DUB,
        self::DUB_TECHNO,
        self::DUBSTEP,
        self::DUBWISE,
        self::DUTCH_HOUSE,
        self::EAST_COAST_RAP,
        self::EASY_LISTENING,
        self::ELECTRO2,
        self::ELECTROHOUSE,
        self::ELECTRO_PROGRESSIVE,
        self::ELECTRO_SWING,
        self::ELECTRO_TECHNO,
        self::ELECTRO_PUNK,
        self::ELECTROCLASH,
        self::EBM,
        self::EURO_TECHNO,
        self::EURO_TRANCE,
        self::EURODANCE,
        self::EXPERIMENTAL,
        self::FIDGET,
        self::FLORIDA_BREAKS,
        self::FOOTWORK,
        self::FREESTYLE,
        self::FRENCH_ELECTRO,
        self::FRENCH_HOUSE,
        self::FRENCHCORE,
        self::FULL_ON,
        self::FUNK,
        self::FUNKY_BREAKS,
        self::FUNKY_HOUSE,
        self::FUNKY_TECHNO,
        self::FUTURE_BASS,
        self::FUTURE_GARAGE,
        self::FUTURE_HOUSE,
        self::FUTUREPOP,
        self::G_FUNK,
        self::G_HOUSE,
        self::GANGSTA_RAP,
        self::GARAGE,
        self::GHETTO_HOUSE,
        self::GHETTOTECH,
        self::GLAM_ROCK,
        self::GLITCH,
        self::GLITCH_HOP,
        self::GOA,
        self::GOTHIC_ROCK,
        self::GRIME,
        self::GRUNGE,
        self::HAPPY_HARDCORE,
        self::HARDHOUSE,
        self::HARD_IDM,
        self::HARD_ROCK,
        self::HARD_TECHNO,
        self::HARDTRANCE,
        self::HARDCORE,
        self::HARDCORE_CONTINUUM,
        self::HARDCORE_RAP,
        self::HARDSTEP,
        self::HARDSTYLE,
        self::HEAVY_METAL,
        self::HI_NRG,
        self::HIP_HOUSE,
        self::HIP_HOP,
        self::HORRORCORE,
        self::HOUSE,
        self::IDM,
        self::ILLBIENT,
        self::INDIE_DANCE,
        self::INDIE_ROCK,
        self::INDIETRONICA,
        self::INDUSTRIAL,
        self::INDUSTRIAL_TECHNO,
        self::INSTRUMENTAL,
        self::INTELLIGENT,
        self::ITALO_DISCO,
        self::JAZZ,
        self::JAZZ_RAP,
        self::JAZZSTEP,
        self::JUMP_UP,
        self::JUMPSTYLE,
        self::JUNGLE,
        self::KRAUTROCK,
        self::LATIN_HOUSE,
        self::LATIN_POP,
        self::LEFT_FIELD_HOUSE,
        self::LEFTFIELD_BASS,
        self::LIQUID_FUNK,
        self::LIVE_LOOPING,
        self::LOFI,
        self::LOUNGE,
        self::LOWERCASE,
        self::MELBOURNE_BOUNCE,
        self::MELODIC_TRANCE,
        self::MIAMI_BASS,
        self::MICROHOUSE,
        self::MINIMAL_TECHNO,
        self::MINIMAL_PSYTRANCE,
        self::MOOMBAHCORE,
        self::MOOMBAHSOUL,
        self::MOOMBAHTON,
        self::MUSIC_CONCRETE,
        self::NEO_SOUL,
        self::NEOTRANCE,
        self::NEUROFUNK,
        self::NEW_AGE,
        self::NEW_BEAT,
        self::NEW_RAVE,
        self::NOISE,
        self::NORTEC,
        self::NU_BREAKS,
        self::NU_DISCO,
        self::NU_JAZZ,
        self::NU_METAL,
        self::OLD_SCHOOL_RAP,
        self::POP,
        self::POP_RAP,
        self::POP_ROCK,
        self::POST_DUBSTEP,
        self::POST_PUNK,
        self::POST_ROCK,
        self::POWER_NOISE,
        self::PROGRESSIVE_BREAKS,
        self::PROGRESSIVE_HOUSE,
        self::PROGRESSIVE_TRANCE,
        self::PROGRESSIVE_ROCK,
        self::PSY_CHILL,
        self::PSYCHEDELIC,
        self::PSYBIENT,
        self::PSY_BREAKS,
        self::PSYCHEDELIC_ROCK,
        self::PUMPING,
        self::PUNK_ROCK,
        self::RNB,
        self::RAGGAJUNGLE,
        self::RAGGACORE,
        self::RAVE,
        self::REGGAE,
        self::REGGAETON,
        self::ROCK,
        self::ROCK_N_ROLL,
        self::ROCKABILLY,
        self::RUSSIAN_POP,
        self::SAMBASS,
        self::SCHRANZ,
        self::SCOUSE_HOUSE,
        self::SHOEGAZING,
        self::SKA,
        self::SLOW_MOTION_HOUSE_DISCO,
        self::SMOOTH_JAZZ,
        self::SOUL,
        self::SOULFUL_HOUSE,
        self::SOUTHERN_RAP,
        self::SPACE_DISCO,
        self::SPACESYNTH,
        self::SPEED_GARAGE,
        self::SPEEDCORE,
        self::STONER_ROCK,
        self::SURF,
        self::SYNTH_POP,
        self::SYNTHWAVE,
        self::TECHHOUSE,
        self::TECH_TRANCE,
        self::TECHNO,
        self::TECHNOID,
        self::TECHSTEP,
        self::TEEN_POP,
        self::TRANCE,
        self::TRANCESTEP,
        self::TRAP,
        self::TRIBAL_HOUSE,
        self::TRIP_HOP,
        self::TROPICAL_HOUSE,
        self::TURNTABLISM,
        self::TWERK,
        self::UK_FUNKY,
        self::UK_GARAGE,
        self::UPLIFTING_TRANCE,
        self::VOCAL_HOUSE,
        self::VOCAL_TRANCE,
        self::WEST_COAST_RAP,
        self::WITCH_HOUSE,
        self::WONKY,
        self::YORKSHIRE_BLEEPS_AND_BASS,
        self::ANALYTICS,
        self::INTERVIEW,
        self::NEWS,
        self::REVIEW,
        self::TALK,
        self::ELECTROPOP,
        self::DISCO_FUNK,
        self::BOOTY_BREAKS,
        self::INDIE,
        self::FUTURE_JAZZ,
        self::MINIMAL_HOUSE,
        self::BUMPING,
    ];

    public const NAME_LIST = [
        self::_2_STEP_NAME,
        self::_8BIT_NAME,
        self::ABSTRACT_HIP_HOP_NAME,
        self::ACID_NAME,
        self::ACID_BREAKS_NAME,
        self::ACID_HOUSE_NAME,
        self::ACID_JAZZ_NAME,
        self::ACID_TECHNO_NAME,
        self::ACID_TRANCE_NAME,
        self::ACOUSTIC_NAME,
        self::AFRO_HOUSE_NAME,
        self::AGGROTECH_NAME,
        self::ALTERNATIVE_RAP_NAME,
        self::ALTERNATIVE_ROCK_NAME,
        self::AMBIENT_NAME,
        self::AMBIENT_BREAKS_NAME,
        self::AMBIENT_DUB_NAME,
        self::AMBIENT_HOUSE_NAME,
        self::ART_ROCK_NAME,
        self::ATMOSPHERIC_BREAKS_NAME,
        self::BAILEFUNK_NAME,
        self::BALEARIC_BEAT_NAME,
        self::BASS_HOUSE_NAME,
        self::BASSLINE_NAME,
        self::BEATBOXING_NAME,
        self::BERLIN_SCHOOL_NAME,
        self::BIG_BEAT_NAME,
        self::BIG_ROOM_HOUSE_NAME,
        self::BLUES_NAME,
        self::BOUNCY_TECHNO_NAME,
        self::BREAKCORE_NAME,
        self::BREAK_BEAT_NAME,
        self::BREAKSTEP_NAME,
        self::BRITISH_RAP_NAME,
        self::BRITPOP_NAME,
        self::BROKENBEAT_NAME,
        self::BROSTEP_NAME,
        self::CHICAGO_HOUSE_NAME,
        self::CHILLOUT_NAME,
        self::CHILLSTEP_NAME,
        self::CHILLWAVE_NAME,
        self::CHIPTUNE_NAME,
        self::CLASSICAL_CROSSOVER_NAME,
        self::CLUB_HOUSE_NAME,
        self::COMEDY_RAP_NAME,
        self::COMPLEXTRO_NAME,
        self::CRUNK_NAME,
        self::DANCE_POP_NAME,
        self::DANCECORE_NAME,
        self::DANCEHALL_NAME,
        self::DARK_AMBIENT_NAME,
        self::DARK_PROGRESSIVE_NAME,
        self::DARK_PSY_TRANCE_NAME,
        self::DARKSTEP_NAME,
        self::DEEP_HOUSE_NAME,
        self::DEEP_TECHNO_NAME,
        self::DETROIT_TECHNO_NAME,
        self::DIGITAL_HARDCORE_NAME,
        self::DIRTY_RAP_NAME,
        self::DISCO_NAME,
        self::DISCO_HOUSE_NAME,
        self::DISS_NAME,
        self::DOWNTEMPO_NAME,
        self::DRILL_N_BASS_NAME,
        self::DNB_NAME,
        self::DRUMFUNK_NAME,
        self::DUB_NAME,
        self::DUB_TECHNO_NAME,
        self::DUBSTEP_NAME,
        self::DUBWISE_NAME,
        self::DUTCH_HOUSE_NAME,
        self::EAST_COAST_RAP_NAME,
        self::EASY_LISTENING_NAME,
        self::ELECTRO2_NAME,
        self::ELECTROHOUSE_NAME,
        self::ELECTRO_PROGRESSIVE_NAME,
        self::ELECTRO_SWING_NAME,
        self::ELECTRO_TECHNO_NAME,
        self::ELECTRO_PUNK_NAME,
        self::ELECTROCLASH_NAME,
        self::EBM_NAME,
        self::EURO_TECHNO_NAME,
        self::EURO_TRANCE_NAME,
        self::EURODANCE_NAME,
        self::EXPERIMENTAL_NAME,
        self::FIDGET_NAME,
        self::FLORIDA_BREAKS_NAME,
        self::FOOTWORK_NAME,
        self::FREESTYLE_NAME,
        self::FRENCH_ELECTRO_NAME,
        self::FRENCH_HOUSE_NAME,
        self::FRENCHCORE_NAME,
        self::FULL_ON_NAME,
        self::FUNK_NAME,
        self::FUNKY_BREAKS_NAME,
        self::FUNKY_HOUSE_NAME,
        self::FUNKY_TECHNO_NAME,
        self::FUTURE_BASS_NAME,
        self::FUTURE_GARAGE_NAME,
        self::FUTURE_HOUSE_NAME,
        self::FUTUREPOP_NAME,
        self::G_FUNK_NAME,
        self::G_HOUSE_NAME,
        self::GANGSTA_RAP_NAME,
        self::GARAGE_NAME,
        self::GHETTO_HOUSE_NAME,
        self::GHETTOTECH_NAME,
        self::GLAM_ROCK_NAME,
        self::GLITCH_NAME,
        self::GLITCH_HOP_NAME,
        self::GOA_NAME,
        self::GOTHIC_ROCK_NAME,
        self::GRIME_NAME,
        self::GRUNGE_NAME,
        self::HAPPY_HARDCORE_NAME,
        self::HARDHOUSE_NAME,
        self::HARD_IDM_NAME,
        self::HARD_ROCK_NAME,
        self::HARD_TECHNO_NAME,
        self::HARDTRANCE_NAME,
        self::HARDCORE_NAME,
        self::HARDCORE_CONTINUUM_NAME,
        self::HARDCORE_RAP_NAME,
        self::HARDSTEP_NAME,
        self::HARDSTYLE_NAME,
        self::HEAVY_METAL_NAME,
        self::HI_NRG_NAME,
        self::HIP_HOUSE_NAME,
        self::HIP_HOP_NAME,
        self::HORRORCORE_NAME,
        self::HOUSE_NAME,
        self::IDM_NAME,
        self::ILLBIENT_NAME,
        self::INDIE_DANCE_NAME,
        self::INDIE_ROCK_NAME,
        self::INDIETRONICA_NAME,
        self::INDUSTRIAL_NAME,
        self::INDUSTRIAL_TECHNO_NAME,
        self::INSTRUMENTAL_NAME,
        self::INTELLIGENT_NAME,
        self::ITALO_DISCO_NAME,
        self::JAZZ_NAME,
        self::JAZZ_RAP_NAME,
        self::JAZZSTEP_NAME,
        self::JUMP_UP_NAME,
        self::JUMPSTYLE_NAME,
        self::JUNGLE_NAME,
        self::KRAUTROCK_NAME,
        self::LATIN_HOUSE_NAME,
        self::LATIN_POP_NAME,
        self::LEFT_FIELD_HOUSE_NAME,
        self::LEFTFIELD_BASS_NAME,
        self::LIQUID_FUNK_NAME,
        self::LIVE_LOOPING_NAME,
        self::LOFI_NAME,
        self::LOUNGE_NAME,
        self::LOWERCASE_NAME,
        self::MELBOURNE_BOUNCE_NAME,
        self::MELODIC_TRANCE_NAME,
        self::MIAMI_BASS_NAME,
        self::MICROHOUSE_NAME,
        self::MINIMAL_TECHNO_NAME,
        self::MINIMAL_PSYTRANCE_NAME,
        self::MOOMBAHCORE_NAME,
        self::MOOMBAHSOUL_NAME,
        self::MOOMBAHTON_NAME,
        self::MUSIC_CONCRETE_NAME,
        self::NEO_SOUL_NAME,
        self::NEOTRANCE_NAME,
        self::NEUROFUNK_NAME,
        self::NEW_AGE_NAME,
        self::NEW_BEAT_NAME,
        self::NEW_RAVE_NAME,
        self::NOISE_NAME,
        self::NORTEC_NAME,
        self::NU_BREAKS_NAME,
        self::NU_DISCO_NAME,
        self::NU_JAZZ_NAME,
        self::NU_METAL_NAME,
        self::OLD_SCHOOL_RAP_NAME,
        self::POP_NAME,
        self::POP_RAP_NAME,
        self::POP_ROCK_NAME,
        self::POST_DUBSTEP_NAME,
        self::POST_PUNK_NAME,
        self::POST_ROCK_NAME,
        self::POWER_NOISE_NAME,
        self::PROGRESSIVE_BREAKS_NAME,
        self::PROGRESSIVE_HOUSE_NAME,
        self::PROGRESSIVE_TRANCE_NAME,
        self::PROGRESSIVE_ROCK_NAME,
        self::PSY_CHILL_NAME,
        self::PSYCHEDELIC_NAME,
        self::PSYBIENT_NAME,
        self::PSY_BREAKS_NAME,
        self::PSYCHEDELIC_ROCK_NAME,
        self::PUMPING_NAME,
        self::PUNK_ROCK_NAME,
        self::RNB_NAME,
        self::RAGGAJUNGLE_NAME,
        self::RAGGACORE_NAME,
        self::RAVE_NAME,
        self::REGGAE_NAME,
        self::REGGAETON_NAME,
        self::ROCK_NAME,
        self::ROCK_N_ROLL_NAME,
        self::ROCKABILLY_NAME,
        self::RUSSIAN_POP_NAME,
        self::SAMBASS_NAME,
        self::SCHRANZ_NAME,
        self::SCOUSE_HOUSE_NAME,
        self::SHOEGAZING_NAME,
        self::SKA_NAME,
        self::SLOW_MOTION_HOUSE_DISCO_NAME,
        self::SMOOTH_JAZZ_NAME,
        self::SOUL_NAME,
        self::SOULFUL_HOUSE_NAME,
        self::SOUTHERN_RAP_NAME,
        self::SPACE_DISCO_NAME,
        self::SPACESYNTH_NAME,
        self::SPEED_GARAGE_NAME,
        self::SPEEDCORE_NAME,
        self::STONER_ROCK_NAME,
        self::SURF_NAME,
        self::SYNTH_POP_NAME,
        self::SYNTHWAVE_NAME,
        self::TECHHOUSE_NAME,
        self::TECH_TRANCE_NAME,
        self::TECHNO_NAME,
        self::TECHNOID_NAME,
        self::TECHSTEP_NAME,
        self::TEEN_POP_NAME,
        self::TRANCE_NAME,
        self::TRANCESTEP_NAME,
        self::TRAP_NAME,
        self::TRIBAL_HOUSE_NAME,
        self::TRIP_HOP_NAME,
        self::TROPICAL_HOUSE_NAME,
        self::TURNTABLISM_NAME,
        self::TWERK_NAME,
        self::UK_FUNKY_NAME,
        self::UK_GARAGE_NAME,
        self::UPLIFTING_TRANCE_NAME,
        self::VOCAL_HOUSE_NAME,
        self::VOCAL_TRANCE_NAME,
        self::WEST_COAST_RAP_NAME,
        self::WITCH_HOUSE_NAME,
        self::WONKY_NAME,
        self::YORKSHIRE_BLEEPS_AND_BASS_NAME,
        self::ANALYTICS_NAME,
        self::INTERVIEW_NAME,
        self::NEWS_NAME,
        self::REVIEW_NAME,
        self::TALK_NAME,
        self::ELECTROPOP_NAME,
        self::DISCO_FUNK_NAME,
        self::BOOTY_BREAKS_NAME,
        self::INDIE_NAME,
        self::FUTURE_JAZZ_NAME,
        self::MINIMAL_HOUSE_NAME,
        self::BUMPING_NAME,
    ];

    public const BACK_VOCALS = "back_vocals";
    public const BASS = "bass";
    public const BELLS = "bells";
    public const CLAP_SNAP = "clap_snap";
    public const CONGA_BONGO = "conga_bongo";
    public const CONSTRUCTION_KIT = "construction_kit";
    public const CRASH = "crash";
    public const DRUM_KIT = "drum_kit";
    public const DRUMS = "drums";
    public const ETHNIC = "ethnic";
    public const EXECUTIVE = "executive";
    public const EXTRA = "extra";
    public const ACP_WOMAN = "acp_woman";
    public const FX = "fx";
    public const FX_MULTITRACK = "fx_multitrack";
    public const GUITARS = "guitars";
    public const HAT = "hat";
    public const HIT = "hit";
    public const IMPULSES = "impulses";
    public const KICK = "kick";
    public const LEADS = "leads";
    public const LOOP = "loop";
    public const ACP_MAN = "acp_man";
    public const MIDI = "midi";
    public const MISC_PERC = "misc_perc";
    public const MISC_SAMPLES = "misc_samples";
    public const MULTITRACK_MASTER = "multitrack_master";
    public const ONE_SHOT = "one_shot";
    public const PADS = "pads";
    public const ACP_PHRASE = "acp_phrase";
    public const PRESET = "preset";
    public const PROGRAM_FILES = "program_files";
    public const PROJECT = "project";
    public const REC_STUFF_FILELD_REC = "rec_stuff_fileld_rec";
    public const SMP_REMIXPACK = "smp_remixpack";
    public const REVERSE_STUFF_SWEEP_NOISE = "reverse_stuff_sweep_noise";
    public const RIDE_CYMBAL = "ride_cymbal";
    public const SAMPLED_STUFF = "sampled_stuff";
    public const SCRATCH_KIT = "scratch_kit";
    public const SNARE = "snare";
    public const ACP_SONG = "acp_song";
    public const ACP_ROBOT = "acp_robot";
    public const TOM = "tom";
    public const TOOL = "tool";
    public const VOCALS = "vocals";
    public const VOICE_FX = "voice_fx";
    public const SAMPLES_ALBUM = 'smp_album';

    public const BACK_VOCALS_NAME = "Back vocals";
    public const BASS_NAME = "Bass";
    public const BELLS_NAME = "Bells";
    public const CLAP_SNAP_NAME = "Clap, Snap";
    public const CONGA_BONGO_NAME = "Conga, Bongo";
    public const CONSTRUCTION_KIT_NAME = "Construction kit";
    public const CRASH_NAME = "Crash";
    public const DRUM_KIT_NAME = "Drum kit";
    public const DRUMS_NAME = "Drums";
    public const ETHNIC_NAME = "Ethnic";
    public const EXECUTIVE_NAME = "Executive (plugins, patches, ensembles)";
    public const EXTRA_NAME = "Extra";
    public const ACP_WOMAN_NAME = "Female vocals";
    public const FX_NAME = "Fx";
    public const FX_MULTITRACK_NAME = "Fx";
    public const GUITARS_NAME = "Guitars";
    public const HAT_NAME = "Hat";
    public const HIT_NAME = "Hit";
    public const IMPULSES_NAME = "Impulses";
    public const KICK_NAME = "Kick";
    public const LEADS_NAME = "Leads (synths, keys)";
    public const LOOP_NAME = "Loop";
    public const ACP_MAN_NAME = "Male vocals";
    public const MIDI_NAME = "Midi";
    public const MISC_PERC_NAME = "Misc Perc";
    public const MISC_SAMPLES_NAME = "Misc samples";
    public const MULTITRACK_MASTER_NAME = "Multitrack master";
    public const ONE_SHOT_NAME = "One Shot";
    public const PADS_NAME = "Pads";
    public const ACP_PHRASE_NAME = "Phrase";
    public const PRESET_NAME = "Preset";
    public const PROGRAM_FILES_NAME = "Program files";
    public const PROJECT_NAME = "Project";
    public const REC_STUFF_FILELD_REC_NAME = "Recording stuff, fileld recordings";
    public const SMP_REMIXPACK_NAME = "Remix pack";
    public const REVERSE_STUFF_SWEEP_NOISE_NAME = "Reverse stuff, sweep, noise";
    public const RIDE_CYMBAL_NAME = "Ride, Cymbal";
    public const SAMPLED_STUFF_NAME = "Sampled stuff";
    public const SCRATCH_KIT_NAME = "Scratch kit";
    public const SNARE_NAME = "Snare";
    public const ACP_SONG_NAME = "Song";
    public const ACP_ROBOT_NAME = "Synthesized vocals";
    public const TOM_NAME = "Tom";
    public const TOOL_NAME = "Tool";
    public const VOCALS_NAME = "Vocals";
    public const VOICE_FX_NAME = "Voice Fx";
    public const SAMPLES_ALBUM_NAME = 'Album';

    public const MUSICIAN_TOOL_MAPPING = [
        self::BACK_VOCALS => self::BACK_VOCALS_NAME,
        self::BASS => self::BASS_NAME,
        self::BELLS => self::BELLS_NAME,
        self::CLAP_SNAP => self::CLAP_SNAP_NAME,
        self::CONGA_BONGO => self::CONGA_BONGO_NAME,
        self::CONSTRUCTION_KIT => self::CONSTRUCTION_KIT_NAME,
        self::CRASH => self::CRASH_NAME,
        self::DRUM_KIT => self::DRUM_KIT_NAME,
        self::DRUMS => self::DRUMS_NAME,
        self::ETHNIC => self::ETHNIC_NAME,
        self::EXECUTIVE => self::EXECUTIVE_NAME,
        self::EXTRA => self::EXTRA_NAME,
        self::ACP_WOMAN => self::ACP_WOMAN_NAME,
        self::FX => self::FX_NAME,
        self::FX_MULTITRACK => self::FX_MULTITRACK_NAME,
        self::GUITARS => self::GUITARS_NAME,
        self::HAT => self::HAT_NAME,
        self::HIT => self::HIT_NAME,
        self::IMPULSES => self::IMPULSES_NAME,
        self::KICK => self::KICK_NAME,
        self::LEADS => self::LEADS_NAME,
        self::LOOP => self::LOOP_NAME,
        self::ACP_MAN => self::ACP_MAN_NAME,
        self::MIDI => self::MIDI_NAME,
        self::MISC_PERC => self::MISC_PERC_NAME,
        self::MISC_SAMPLES => self::MISC_SAMPLES_NAME,
        self::MULTITRACK_MASTER => self::MULTITRACK_MASTER_NAME,
        self::ONE_SHOT => self::ONE_SHOT_NAME,
        self::PADS => self::PADS_NAME,
        self::ACP_PHRASE => self::ACP_PHRASE_NAME,
        self::PRESET => self::PRESET_NAME,
        self::PROGRAM_FILES => self::PROGRAM_FILES_NAME,
        self::PROJECT => self::PROJECT_NAME,
        self::REC_STUFF_FILELD_REC => self::REC_STUFF_FILELD_REC_NAME,
        self::SMP_REMIXPACK => self::SMP_REMIXPACK_NAME,
        self::REVERSE_STUFF_SWEEP_NOISE => self::REVERSE_STUFF_SWEEP_NOISE_NAME,
        self::RIDE_CYMBAL => self::RIDE_CYMBAL_NAME,
        self::SAMPLED_STUFF => self::SAMPLED_STUFF_NAME,
        self::SCRATCH_KIT => self::SCRATCH_KIT_NAME,
        self::SNARE => self::SNARE_NAME,
        self::ACP_SONG => self::ACP_SONG_NAME,
        self::ACP_ROBOT => self::ACP_ROBOT_NAME,
        self::TOM => self::TOM_NAME,
        self::TOOL => self::TOOL_NAME,
        self::VOCALS => self::VOCALS_NAME,
        self::VOICE_FX => self::VOICE_FX_NAME,
        self::SAMPLES_ALBUM => self::SAMPLES_ALBUM_NAME,
    ];

    public const MUSICIAN_TOOL_LIST = [
        self::BACK_VOCALS,
        self::BASS,
        self::BELLS,
        self::CLAP_SNAP,
        self::CONGA_BONGO,
        self::CONSTRUCTION_KIT,
        self::CRASH,
        self::DRUM_KIT,
        self::DRUMS,
        self::ETHNIC,
        self::EXECUTIVE,
        self::EXTRA,
        self::ACP_WOMAN,
        self::FX,
        self::FX_MULTITRACK,
        self::GUITARS,
        self::HAT,
        self::HIT,
        self::IMPULSES,
        self::KICK,
        self::LEADS,
        self::LOOP,
        self::ACP_MAN,
        self::MIDI,
        self::MISC_PERC,
        self::MISC_SAMPLES,
        self::MULTITRACK_MASTER,
        self::ONE_SHOT,
        self::PADS,
        self::ACP_PHRASE,
        self::PRESET,
        self::PROGRAM_FILES,
        self::PROJECT,
        self::REC_STUFF_FILELD_REC,
        self::SMP_REMIXPACK,
        self::REVERSE_STUFF_SWEEP_NOISE,
        self::RIDE_CYMBAL,
        self::SAMPLED_STUFF,
        self::SCRATCH_KIT,
        self::SNARE,
        self::ACP_SONG,
        self::ACP_ROBOT,
        self::TOM,
        self::TOOL,
        self::VOCALS,
        self::VOICE_FX,
        self::SAMPLES_ALBUM,
    ];

    public const MUSICIAN_TOOL_NAME_LIST = [
        self::BACK_VOCALS_NAME,
        self::BASS_NAME,
        self::BELLS_NAME,
        self::CLAP_SNAP_NAME,
        self::CONGA_BONGO_NAME,
        self::CONSTRUCTION_KIT_NAME,
        self::CRASH_NAME,
        self::DRUM_KIT_NAME,
        self::DRUMS_NAME,
        self::ETHNIC_NAME,
        self::EXECUTIVE_NAME,
        self::EXTRA_NAME,
        self::ACP_WOMAN_NAME,
        self::FX_NAME,
        self::FX_MULTITRACK_NAME,
        self::GUITARS_NAME,
        self::HAT_NAME,
        self::HIT_NAME,
        self::IMPULSES_NAME,
        self::KICK_NAME,
        self::LEADS_NAME,
        self::LOOP_NAME,
        self::ACP_MAN_NAME,
        self::MIDI_NAME,
        self::MISC_PERC_NAME,
        self::MISC_SAMPLES_NAME,
        self::MULTITRACK_MASTER_NAME,
        self::ONE_SHOT_NAME,
        self::PADS_NAME,
        self::ACP_PHRASE_NAME,
        self::PRESET_NAME,
        self::PROGRAM_FILES_NAME,
        self::PROJECT_NAME,
        self::REC_STUFF_FILELD_REC_NAME,
        self::SMP_REMIXPACK_NAME,
        self::REVERSE_STUFF_SWEEP_NOISE_NAME,
        self::RIDE_CYMBAL_NAME,
        self::SAMPLED_STUFF_NAME,
        self::SCRATCH_KIT_NAME,
        self::SNARE_NAME,
        self::ACP_SONG_NAME,
        self::ACP_ROBOT_NAME,
        self::TOM_NAME,
        self::TOOL_NAME,
        self::VOCALS_NAME,
        self::VOICE_FX_NAME,
        self::SAMPLES_ALBUM_NAME,
    ];

    public const WITH_COMMA_NAME_LIST = [
        self::CLAP_SNAP_NAME,
        self::CONGA_BONGO_NAME,
        self::EXECUTIVE_NAME,
        self::LEADS_NAME,
        self::REC_STUFF_FILELD_REC_NAME,
        self::REVERSE_STUFF_SWEEP_NOISE_NAME,
        self::RIDE_CYMBAL_NAME,
    ];

    /**
     * @return string[];
     */
    public static function getStylesWithMusicianTools(): array
    {
        return array_merge(self::LIST, self::MUSICIAN_TOOL_LIST);
    }

    /**
     * @return string[];
     */
    public static function getStyleNamesWithMusicianTools(): array
    {
        return array_merge(self::NAME_LIST, self::MUSICIAN_TOOL_NAME_LIST);
    }
}