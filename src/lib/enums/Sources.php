<?php

namespace PromodjSDK\enums;

class Sources
{
    public const CDJ = 'cdj';
    public const ONLY_VINYL = 'only_vinyl';
    public const DIGITAL_VINYL = 'digital_vinyl';
    public const ABLETON = 'ableton';
    public const LAPTOP = 'laptop';
    public const MIDI = 'midi';
    public const LIVE = 'live';
    public const TAPE = 'tape';
    public const ONLY_CD = 'only_cd';
    public const MP3 = 'MP3';
    public const SERATO_DJ = 'serato_dj';
    public const TRAKTOR_SCRATCH = 'traktor_scratch';
    public const VINYL_AND_CD = 'vinyl_and_CD';
    public const TRAKTOR = 'traktor';
    public const OTHER = 'other';

    public const CDJ_NAME = 'CDJ';
    public const ONLY_VINYL_NAME = 'vinyl';
    public const DIGITAL_VINYL_NAME = 'digital vinyl';
    public const ABLETON_NAME = 'Ableton';
    public const LAPTOP_NAME = 'laptop';
    public const MIDI_NAME = 'midi controller';
    public const LIVE_NAME = 'live performance';
    public const TAPE_NAME = 'tape';
    public const ONLY_CD_NAME = 'только CD';
    public const MP3_NAME = 'MP3';
    public const SERATO_DJ_NAME = 'Serato DJ';
    public const TRAKTOR_SCRATCH_NAME = 'Traktor Scratch';
    public const VINYL_AND_CD_NAME = 'винил + CD';
    public const TRAKTOR_NAME = 'Traktor';
    public const OTHER_NAME = 'другое';

    public const LIST = [
        self::CDJ,
        self::ONLY_VINYL,
        self::DIGITAL_VINYL,
        self::ABLETON,
        self::LAPTOP,
        self::MIDI,
        self::LIVE,
        self::TAPE,
        self::ONLY_CD,
        self::MP3,
        self::SERATO_DJ,
        self::TRAKTOR_SCRATCH,
        self::VINYL_AND_CD,
        self::TRAKTOR,
        self::OTHER,
    ];

    public const NAME_LIST = [
        self::CDJ_NAME,
        self::ONLY_VINYL_NAME,
        self::DIGITAL_VINYL_NAME,
        self::ABLETON_NAME,
        self::LAPTOP_NAME,
        self::MIDI_NAME,
        self::LIVE_NAME,
        self::TAPE_NAME,
        self::ONLY_CD_NAME,
        self::MP3,
        self::SERATO_DJ_NAME,
        self::TRAKTOR_SCRATCH_NAME,
        self::VINYL_AND_CD_NAME,
        self::TRAKTOR_NAME,
        self::OTHER_NAME,
    ];

    public const MAPPING = [
        self::CDJ => self::CDJ_NAME,
        self::ONLY_VINYL => self::ONLY_VINYL_NAME,
        self::DIGITAL_VINYL => self::DIGITAL_VINYL_NAME,
        self::ABLETON => self::ABLETON_NAME,
        self::LAPTOP => self::LAPTOP_NAME,
        self::MIDI => self::MIDI_NAME,
        self::LIVE => self::LIVE_NAME,
        self::TAPE => self::TAPE_NAME,
        self::ONLY_CD => self::ONLY_CD_NAME,
        self::MP3 => self::MP3_NAME,
        self::SERATO_DJ => self::SERATO_DJ_NAME,
        self::TRAKTOR_SCRATCH => self::TRAKTOR_SCRATCH_NAME,
        self::VINYL_AND_CD => self::VINYL_AND_CD_NAME,
        self::TRAKTOR => self::TRAKTOR_NAME,
        self::OTHER => self::OTHER_NAME,
    ];
}